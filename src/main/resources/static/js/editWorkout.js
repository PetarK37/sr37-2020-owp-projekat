$(document).ready(function (){
    var error = $("#errorDiv")
    var form = $("form")

    form.submit(function (e){
        if ($('input[name=kindId]:checked').length <= 0){
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "Choose at least one workout kind!"
            error.append(errorMsg)
            e.preventDefault()
        }
    })
})