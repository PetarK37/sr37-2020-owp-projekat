$(document).ready(function (){
    var error = $("#errorDiv")
    var form = $("form")

    form.submit(function (e){
        var discounts = $('input[name=discount]').serializeArray();
        var workouts = new Array();
        var dateStart = new Date($('input[name=dateTimeStart]').val());
        var dateEnd = new Date($('input[name=dateTimeEnd]').val());

        if (dateStart > dateEnd){
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "Start date is after end date!"
            error.append(errorMsg)
            e.preventDefault();
        }

        if (dateStart < new Date()){
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "Start date has passed!"
            error.append(errorMsg)
            e.preventDefault();
        }

        $('input[name=workoutId]').each(function (){
            workouts.push({name: this.name,checked: this.checked});
        });

        if ($('input[name=workoutId]:checked').length <= 0){
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "Choose at least one workout!"
            error.append(errorMsg)
            e.preventDefault();
        }

        for (var i = 0; i < discounts.length; i++){
            if (((discounts[i]["value"] == 0 || discounts[i]["value"] == "" )  && workouts[i]['checked'] == true) || (discounts[i]["value"] != 0 && workouts[i]["checked"] == false)){
                error.find('h2:first').remove();
                var errorMsg = document.createElement("h2")
                errorMsg.style.color = "red";
                errorMsg.innerText = "Please check if you added discounts for all selected workouts(unselected shall remain 0)!"
                error.append(errorMsg)
                e.preventDefault()
            }
        }

    })
})