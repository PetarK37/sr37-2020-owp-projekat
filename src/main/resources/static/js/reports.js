$(document).ready(function (){
    var error = $("#errorDiv")
    var searchFrom = $("#searchForm")
    var dateFrom = $("input[name = 'startDate']")
    var dateTo = $("input[name = 'endDate']")
    var orderForm = $("#orderForm")

    var response;

    searchFrom.submit(function (e){
        if (new Date(dateFrom.val()) > new Date(dateTo.val())){
            e.preventDefault();
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "Start date is after end date!"
            error.append(errorMsg)
        }
        var  params = {
            dateFrom : dateFrom.val(),
            dateTo : dateTo.val()
        }
        response = getReports(params);
        fillInTable(response);
        e.preventDefault();
    })

    orderForm.submit(function (e){
        e.preventDefault()

        var criterium = $("select[name = 'orderCriterium']").val()
        var sort = $("select[name = 'order']").val()
        console.log(criterium + sort)

        if (criterium == "price" && sort == "asc"){
            response["reports"] = response["reports"].sort(priceAsc)
            fillInTable(response)
            console.log(response)
        }else if (criterium == "price" && sort == "desc"){
            response["reports"] = response["reports"].sort(priceDesc)
            fillInTable(response)
            console.log(response)
        }else if(criterium == "numberOfRes" && sort == "asc"){
            response["reports"] = response["reports"].sort(numAsc)
            fillInTable(response)
            console.log(response)
        }else if(criterium == "numberOfRes" && sort == "desc"){
            response["reports"] = response["reports"].sort(numDesc)
            fillInTable(response)
            console.log(response)
        }
    })
})




function getReports(params){
    var retVal = $.ajax({type: "GET", url: "Reports/GetReports" ,data: params, async: false}).responseText
    console.log(JSON.parse(retVal))
    return JSON.parse(retVal)
}


function fillInTable(response){
    if (response["status"] == "ok"){
        var table = $("#reportsTable")
        var error = $("#errorDiv")
        error.find('h2:first').remove();
        $("#reportsTable tbody").remove();
        for (var i = 0; i < response["reports"].length; i++){
            var tbody = document.createElement("tbody");
            var tr = document.createElement("tr");
            table.append(tbody);
            tbody.appendChild(tr);
            var tdWorkout = document.createElement("td");
            var tdInstructors = document.createElement("td");
            var tdNumOfRes = document.createElement("td");
            var tdPrice = document.createElement("td");
            tdWorkout.innerText = response["reports"][i]["workout"]["name"]
            tdInstructors.innerText = response["reports"][i]["workout"]["coaches"];
            tdNumOfRes.innerText = response["reports"][i]["numOfReservations"];
            tdPrice.innerText = response["reports"][i]["totalPrice"]
            tr.appendChild(tdWorkout);
            tr.appendChild(tdInstructors);
            tr.appendChild(tdNumOfRes);
            tr.appendChild(tdPrice);
        }
    }   else {
            var error = $("#errorDiv")
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "There are no reservations fot that period!"
            error.append(errorMsg)
    }
}


function priceAsc(a,b){
    return a["totalPrice"] - b["totalPrice"]
}

function priceDesc(a,b){
    return  b["totalPrice"] - a["totalPrice"]
}

function numAsc(a,b){
    return a["numOfReservations"] - b["numOfReservations"]
}

function numDesc(a,b){
    return b["numOfReservations"] -  a["numOfReservations"]
}