$(document).ready(function (){
    var anonymusCheck = $('input[name=anonymous]')
    var content = $('textarea[name = content]')
    var rating = $('input[name = rating]')
    var workout = $('input[name = workoutId]')
    var user = $('input[name = username]')
    var error = $("#errorDiv")

    function addNew(){
        var contentStr = content.val()
        var ratingNum = rating.val()
        var  workoutId = workout.val()
        var username = user.val()

        var params = {
                workoutId : workoutId,
                content : contentStr,
                rating : ratingNum,
                username : username,
                anonymous  : anonymusCheck.is(":checked")
            }

        $.post("Comments/AddComment",params,function (response){
            if (response["status"] == "ok"){
                window.location.replace("Workouts/UserPage")
            }else{
                error.find('h2:first').remove();
                var errorMsg = document.createElement("h2")
                errorMsg.style.color = "red";
                errorMsg.innerText = "There was problem with saving  comment in database!"
                error.append(errorMsg)
            }
        })
    }

    $("form").submit(function (e){
        e.preventDefault();
        addNew();
    })
})