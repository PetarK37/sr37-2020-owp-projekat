package com.SR37.OWP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class OwpApplication {

	public static void main(String[] args) {
		SpringApplication.run(OwpApplication.class, args);
	}

}
