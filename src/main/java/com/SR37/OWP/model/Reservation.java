package com.SR37.OWP.model;

import java.time.LocalDateTime;

public class Reservation {
    private LocalDateTime dateTime;
    private Schedule schedule;
    private User user;
    private float price;

    public Reservation(LocalDateTime dateTime, Schedule schedule, User user, float price) {
        this.dateTime = dateTime;
        this.schedule = schedule;
        this.user = user;
        this.price = price;
    }

    public Reservation(Schedule schedule, User user,float price) {
        this.dateTime = LocalDateTime.now().withSecond(0).withNano(0);
        this.schedule = schedule;
        this.user = user;
        this.price = price;
    }
    public Reservation() {
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}

