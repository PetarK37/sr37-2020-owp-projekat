package com.SR37.OWP.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SpecialDateDTO {
    LocalDateTime startDate;
    LocalDateTime endDate;
    int workoutId;
    int discount;


    public SpecialDateDTO(LocalDate startDate, LocalDate endDate, int workoutId, int discount) {
        this.startDate = startDate.atStartOfDay();
        this.endDate = endDate.atStartOfDay();
        this.workoutId = workoutId;
        this.discount = discount;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public int getWorkoutId() {
        return workoutId;
    }

    public int getDiscount() {
        return discount;
    }
}
