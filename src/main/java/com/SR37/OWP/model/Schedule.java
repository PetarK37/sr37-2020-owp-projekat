package com.SR37.OWP.model;


import java.time.Duration;
import java.time.LocalDateTime;

public class Schedule {
    private long id;
    private Workout workout;
    private Room room;
    private LocalDateTime dateTime;
    private LocalDateTime endDateTime;
    private long duration; //in minutes

    public Schedule(){};

    public Schedule(long id, Workout workout, Room room, LocalDateTime dateTime, LocalDateTime endDateTime) {
        this.id = id;
        this.workout = workout;
        this.room = room;
        this.dateTime = dateTime;
        this.endDateTime = endDateTime;
        this.duration = Duration.between(dateTime,endDateTime).toMinutes();
    }

    public Schedule(Workout workout, Room room, LocalDateTime dateTime, LocalDateTime endDateTime) {
        this.workout = workout;
        this.room = room;
        this.dateTime = dateTime;
        this.endDateTime = endDateTime;
        this.duration = Duration.between(dateTime,endDateTime).toMinutes();
    }

    public long getId() {
        return id;
    }

    public Workout getWorkout() {
        return workout;
    }

    public Room getRoom() {
        return room;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
