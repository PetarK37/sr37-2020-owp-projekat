package com.SR37.OWP.model;

public class RoomSearchDTO {
    private String id;
    private String sortingCriterium;
    private String sort;

    public RoomSearchDTO(String id, String sortingCriterium, String sort) {
        this.id = id;
        this.sortingCriterium = sortingCriterium;
        this.sort = sort;
    }

    public String getId() {
        return id;
    }

    public String getSortingCriterium() {
        return sortingCriterium;
    }

    public String getSort() {
        return sort;
    }
}
