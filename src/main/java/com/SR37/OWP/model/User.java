package com.SR37.OWP.model;

import com.SR37.OWP.model.enums.EUserRole;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class User {
    private String username;
    private String password;
    private String email;
    private String name;
    private String surname;
    private LocalDate birthday;
    private String address;
    private String phoneNum;
    private LocalDateTime dateTimeOfRegistration;
    private EUserRole userRole;
    private boolean isBlocked;

    public User(){};

    public User(String username, String password, String email, String name, String surname, LocalDate birthday, String address, String phoneNum, LocalDateTime dateTimeOfRegistration, EUserRole userRole,Boolean isBlocked) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.address = address;
        this.phoneNum = phoneNum;
        this.dateTimeOfRegistration = dateTimeOfRegistration;
        this.userRole = userRole;
        this.isBlocked = isBlocked;
    }

    public User(String username, String password, String email, String name, String surname, LocalDate birthday, String address, String phoneNum, EUserRole userRole) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.address = address;
        this.phoneNum = phoneNum;
        this.userRole = userRole;
        this.isBlocked = false;
    }

    public void Update(String username, String email, String name, String surname, LocalDate birthday, String address, String phoneNum, EUserRole userRole){
        this.username = username;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.address = address;
        this.phoneNum = phoneNum;
        this.userRole = userRole;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public LocalDateTime getDateTimeOfRegistration() {
        return dateTimeOfRegistration;
    }

    public EUserRole getUserRole() {
        return userRole;
    }

    public boolean isBlocked() {
        return isBlocked;
    }
}
