package com.SR37.OWP.model;

import com.SR37.OWP.model.enums.EWorkoutType;
import com.SR37.OWP.model.enums.EWorkoutLevel;

import java.util.ArrayList;

public class Workout {
    private long id;
    private String name;
    private String coaches;
    private String description;
    private String imgUrl;
    private ArrayList<WorkoutKind> kinds;
    private float price;
    private EWorkoutType type;
    private float rating;
    private EWorkoutLevel level;

    public Workout(){};

    public Workout(String name, String coaches, String description, String imgUrl, float price, EWorkoutType type, EWorkoutLevel level) {
        this.name = name;
        this.coaches = coaches;
        this.description = description;
        this.imgUrl = imgUrl;
        this.price = price;
        this.type = type;
        this.level = level;
        this.kinds = new ArrayList<>();
    }

    public void setId(long id) {
        this.id = id;
    }

    public Workout(long id, String name, String coaches, String description, String imgUrl, float price, EWorkoutType type, EWorkoutLevel level, float rating) {
        this.id = id;
        this.name = name;
        this.coaches = coaches;
        this.description = description;
        this.imgUrl = imgUrl;
        this.price = price;
        this.type = type;
        this.level = level;
        this.rating = rating;
        this.kinds = new ArrayList<>();
    }

    public Workout(String name, String coaches, String description, String imgUrl, float price, EWorkoutType type, EWorkoutLevel level,ArrayList<WorkoutKind> kinds) {

        this.name = name;
        this.coaches = coaches;
        this.description = description;
        this.imgUrl = imgUrl;
        this.price = price;
        this.type = type;
        this.level = level;
        this.kinds = kinds;
    }

    public void update(String name, String coaches, String description, String img, float price, EWorkoutType type, EWorkoutLevel level,ArrayList<WorkoutKind> kinds) {
        this.name = name;
        this.coaches = coaches;
        this.description = description;
        this.imgUrl = img.equals("") ? this.imgUrl : img;
        this.price = price;
        this.type = type;
        this.level = level;
        this.kinds = kinds;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public String getCoaches() {
        return coaches;
    }

    public String getDescription() {
        return description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public ArrayList<WorkoutKind> getKinds() {
        return kinds;
    }

    public float getPrice() {
        return price;
    }

    public EWorkoutType getType() {
        return type;
    }

    public float getRating() {
        return rating;
    }

    public EWorkoutLevel getLevel() {
        return level;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
