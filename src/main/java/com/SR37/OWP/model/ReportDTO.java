package com.SR37.OWP.model;

import java.util.ArrayList;

public class ReportDTO {
    private Workout workout;
    private int numOfReservations;
    private float totalPrice;

    public ReportDTO(Workout workout, int numOfReservations, float totalPrice) {
        this.workout = workout;
        this.numOfReservations = numOfReservations;
        this.totalPrice = totalPrice;
    }

    public Workout getWorkout() {
        return workout;
    }

    public int getNumOfReservations() {
        return numOfReservations;
    }

    public float getTotalPrice() {
        return totalPrice;
    }
}
