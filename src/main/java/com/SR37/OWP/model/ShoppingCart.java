package com.SR37.OWP.model;

import java.util.ArrayList;

public class ShoppingCart {
    private ArrayList<Schedule> schedules;
    private float loyaltyPoints;
    private float price;



    public ShoppingCart() {
        this.schedules = new ArrayList<>();
        this.loyaltyPoints = 0;
        this.price = 0;
    }

    public ShoppingCart(ArrayList<Schedule> schedules) {
        this.schedules = schedules;
        this.loyaltyPoints = 0;
        for (Schedule s: this.schedules) {
            this.price+= s.getWorkout().getPrice();
        }
    }

    public ShoppingCart(ArrayList<Schedule> schedules, float loyaltyPoints) {
        this.schedules = schedules;
        this.loyaltyPoints = loyaltyPoints;
        for (Schedule s: this.schedules) {
            s.getWorkout().setPrice(s.getWorkout().getPrice()-(s.getWorkout().getPrice() * ((loyaltyPoints*5)/100)));
            this.price+= s.getWorkout().getPrice();
        }
    }

    public void  recalculatePrice(float loyaltyPoints){
        this.loyaltyPoints = loyaltyPoints;
        this.price = 0;
        for (Schedule s: this.schedules) {
            float price =s.getWorkout().getPrice() - (s.getWorkout().getPrice() * ((loyaltyPoints*5)/100));
            s.getWorkout().setPrice(price);
            this.price+= s.getWorkout().getPrice();
        }
    }

    public void  recaluclateSpecialPoints(float specialPoints,int id){
        this.price = 0;
        for (Schedule s: this.schedules) {
            if (s.getId() == id){
                float price = s.getWorkout().getPrice() - (s.getWorkout().getPrice() * (specialPoints/100));
                s.getWorkout().setPrice(price);
                this.price+= s.getWorkout().getPrice();
            }else {
                this.price+= s.getWorkout().getPrice();
            }
        }
    }

    public ArrayList<Schedule> getSchedules() {
        return schedules;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void addSchedule(Schedule schedule){
        this.schedules.add(schedule);
        this.price+= schedule.getWorkout().getPrice();


    }

    public float getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(float loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }
}
