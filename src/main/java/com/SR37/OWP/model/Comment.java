package com.SR37.OWP.model;

import com.SR37.OWP.model.enums.EcommentStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Comment {
    private long id;
    private String content;
    private float rating;
    private LocalDateTime date;
    private String author;
    private Workout workout;
    private EcommentStatus status;

    public Comment() { };

    public Comment(String content, float rating, String author,Workout workout) {
        this.content = content;
        this.rating = rating;
        this.date = LocalDateTime.now();
        this.author = author;
        this.workout = workout;
    }

    public Workout getWorkout() {
        return workout;
    }

    public Comment(long id, String content, float rating, LocalDateTime date, String author, Workout workout , EcommentStatus status) {
        this.id = id;
        this.content = content;
        this.rating = rating;
        this.date = date;
        this.author = author;
        this.status = status;
        this.workout = workout;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public float getRating() {
        return rating;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getAuthor() {
        return author;
    }

    public EcommentStatus getStatus() {
        return status;
    }
}
