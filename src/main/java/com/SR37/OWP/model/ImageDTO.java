package com.SR37.OWP.model;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class ImageDTO {
    private String path;
    private String filename;
    private  byte[] bytes;

    public ImageDTO(MultipartFile imgFile) throws IOException {
        this.path = "/img/" + imgFile.getOriginalFilename();
        this.filename = imgFile.getOriginalFilename();
        this.bytes = imgFile.getBytes();
    }

    public String getPath() {
        return path;
    }

    public String getFilename() {
        return filename;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
