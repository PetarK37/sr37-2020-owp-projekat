package com.SR37.OWP.model;

public class WorkoutSearchDTO {
    private String name;
    private String type;
    private String level;
    private String instructors;
    private String kindName;
    private float priceFrom;
    private float priceTo;
    private String sortingCriterium;
    private String sort;

    public String getSortingCriterium() {
        return sortingCriterium;
    }

    public String getSort() {
        return sort;
    }



    public WorkoutSearchDTO(String name, String type, String level, String instructors, String kindName, float priceFrom, float priceTo, String sortingCriterium, String sort) {
        this.name = name;
        this.type = type;
        this.level = level;
        this.instructors = instructors;
        this.kindName = kindName;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.sortingCriterium = sortingCriterium;
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getLevel() {
        return level;
    }

    public String getInstructors() {
        return instructors;
    }

    public String getKindName() {
        return kindName;
    }

    public float getPriceFrom() {
        return priceFrom;
    }

    public float getPriceTo() {
        return priceTo;
    }
}
