package com.SR37.OWP.model;

import com.SR37.OWP.model.enums.EUserRole;

public class UserSearchDTO {
    private String username;
    private EUserRole userRole;
    private String sortingCriterium;
    private String sort;

    public UserSearchDTO(String username, EUserRole userRole, String sortingCriterium, String sort) {
        this.username = username;
        this.userRole = userRole;
        this.sortingCriterium = sortingCriterium;
        this.sort = sort;
    }

    public String getUsername() {
        return username;
    }

    public EUserRole getUserRole() {
        return userRole;
    }

    public String getSortingCriterium() {
        return sortingCriterium;
    }

    public String getSort() {
        return sort;
    }
}
