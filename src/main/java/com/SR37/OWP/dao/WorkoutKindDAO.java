package com.SR37.OWP.dao;

import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutKind;
import jdk.jshell.Snippet;

import java.util.ArrayList;
import java.util.List;

public interface WorkoutKindDAO {

    public ArrayList<WorkoutKind> findAll();
    public ArrayList<WorkoutKind> findAll(List<Integer> ids);
    public int[] saveAllKindsPerWorkout(Workout workout);
    public WorkoutKind findOne(int id);
}
