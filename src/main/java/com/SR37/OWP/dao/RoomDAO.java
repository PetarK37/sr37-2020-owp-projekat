package com.SR37.OWP.dao;

import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.RoomSearchDTO;
import com.SR37.OWP.model.Schedule;

import java.util.ArrayList;
import java.util.LinkedList;

public interface RoomDAO {
    public ArrayList<Room> findAll();
    public ArrayList<Room> findAll(RoomSearchDTO roomSearchDTO);
    public boolean roomIsFree(Room room);
    public Room finOne(String roomId);
    public boolean remove(String roomId);
    public boolean save(Room room);
    public boolean update(Room room);
    public boolean roomIsFree(Schedule schedule);
    public boolean canEditRoom(String roomId);
}
