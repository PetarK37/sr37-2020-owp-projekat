package com.SR37.OWP.dao;

import com.SR37.OWP.model.Workout;

import java.util.ArrayList;

public interface WishListDAO {
    public ArrayList<Workout> loadWishedWorkouts(String username);
    public boolean addToWishlist(int workoutId,String username);
    public boolean remove(int workoutId, String username);
}
