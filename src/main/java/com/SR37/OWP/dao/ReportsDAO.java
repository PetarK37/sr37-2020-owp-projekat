package com.SR37.OWP.dao;

import com.SR37.OWP.model.ReportDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public interface ReportsDAO {
    public ArrayList<ReportDTO> findAllByDates(LocalDateTime startDate, LocalDateTime endDate);
}
