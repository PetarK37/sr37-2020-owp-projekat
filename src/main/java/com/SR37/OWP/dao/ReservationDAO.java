package com.SR37.OWP.dao;

import com.SR37.OWP.model.Reservation;
import com.SR37.OWP.model.Schedule;

import java.time.LocalDateTime;
import java.util.ArrayList;

public interface ReservationDAO {
    public Reservation findOne(String username,Long scheduleId);
    public Reservation findByWorkout(String username,Integer workoutId);
    public ArrayList<Reservation> findAllByRange(LocalDateTime startDate, LocalDateTime endDate);
    public ArrayList<Reservation> findAll(String username);
    public boolean save(Reservation reservation);
    public boolean delete(Reservation reservation);
    public boolean canReserve(Schedule schedule,String username);
}
