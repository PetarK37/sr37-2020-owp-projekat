package com.SR37.OWP.dao;

import com.SR37.OWP.model.Schedule;

import java.util.ArrayList;

public interface ScheduleDAO {
    public ArrayList<Schedule> findAll();
    public Schedule findOne(long schedualeId);
    public ArrayList<Schedule> findAllCurrent();
    public ArrayList<Schedule> findAll(int workoutId);
    public ArrayList<Schedule> findAllCurrent(int workoutId);
    public boolean Save(Schedule schedule);
    public boolean UpdateStatus(Schedule schedule);
    public boolean DeleteStatus(Schedule schedule);

}
