package com.SR37.OWP.dao;

import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.SpecialDateDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;


public interface SpecialDateDAO {
    public Float findDiscountForDay(Schedule schedule);
    public boolean isSpecialDate(ShoppingCart shoppingCart);
    public boolean save(SpecialDateDTO specialDateDTO);
}
