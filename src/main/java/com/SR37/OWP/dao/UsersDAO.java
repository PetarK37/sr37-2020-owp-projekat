package com.SR37.OWP.dao;

import com.SR37.OWP.model.User;
import com.SR37.OWP.model.UserSearchDTO;

import java.util.ArrayList;

public interface UsersDAO {

    public ArrayList<User> findAll();
    public ArrayList<User> findAll(UserSearchDTO userSearchDTO);
    public User findOne(String username);
    public boolean usernameIsFree(String username);
    public boolean emailIsFree(String email);
    public boolean findOne(String username, String email);
    public int findNotBlocked(String username,String password);
    public boolean save(User user);
    public boolean applyBlocStatus(boolean status,String username);
    public boolean update(User user,String begginingUsername);
    public boolean update(String password,String username);
}
