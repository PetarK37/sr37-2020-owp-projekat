package com.SR37.OWP.dao;

import com.SR37.OWP.model.User;

import java.util.ArrayList;

public interface LoyaltyCardDAO {
    public Integer getMyLoyaltyPoints(String username);
    public ArrayList<User> getAllApplicants();
    public  boolean hasApplied(String username);
    public boolean apply(String username);
    public boolean acceptApplication(String id);
    public boolean declineApplication(String id);
    public boolean addPoints(String username,int points);
    public boolean removePoints(String username,int points);
}
