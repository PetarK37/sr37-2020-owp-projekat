package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.WishListDAO;
import com.SR37.OWP.dao.WorkoutDAO;
import com.SR37.OWP.model.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository
public class WishlistDAOImpl implements WishListDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WorkoutDAO workoutDAO;

    @Override
    public ArrayList<Workout> loadWishedWorkouts(String username) {
        ArrayList<Workout> retVal =  new ArrayList<Workout>();

        String sql = "select workoutId from wishlist where userId = ? ;";
        for (Integer workoutId : jdbcTemplate.queryForList(sql,Integer.class,username)){
            retVal.add(workoutDAO.findOne(workoutId));
        }
        return retVal;
    }

    @Override
    public boolean addToWishlist(int workoutId,String username) {
        String sql ="insert into wishlist(workoutId,userId) values (?,?);";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setInt(index++,workoutId);
                preparedStatement.setString(index++,username);

                return preparedStatement;
            };
    };
        if(isInList(workoutId,username)){
            return jdbcTemplate.update(preparedStatementCreator) == 1;
        }
        return false;
    }

    @Override
    public boolean remove(int workoutId, String username) {
        String sql ="delete from wishlist where workoutId = ? and userId = ?;";
        return jdbcTemplate.update(sql,workoutId,username) == 1;
    }

    private boolean isInList(int workoutId,String username){
        String sql = "select count(*) from wishlist where userId = ? and workoutId = ?;";

        return jdbcTemplate.queryForObject(sql,Integer.class,username,workoutId) == 0;
    }
}
