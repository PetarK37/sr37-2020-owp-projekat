package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.*;
import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.enums.EWorkoutType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ShoppingCartDAOImpl implements ShoppingCartDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RoomDAO roomDAO;

    @Autowired
    private WorkoutDAO workoutDAO;

    @Autowired
    private SpecialDateDAO specialDateDAO;




    private class ShoppingCartRowCallbackHandler implements RowCallbackHandler {
       private ArrayList<Schedule> schedules = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {


            String user = rs.getString("userId");
            Long scheduleId = rs.getLong("schedualeId");
            LocalDateTime startDateTime = rs.getTimestamp("_dateTime").toLocalDateTime();
            LocalDateTime endDateTime = rs.getTimestamp("endDateTime").toLocalDateTime();

            String roomID = rs.getString("roomId");
            int id = rs.getInt("workoutId");


            Workout workout = workoutDAO.findOne(id);
            Room room = roomDAO.finOne(roomID);
            Schedule schedule = new Schedule(scheduleId, workout, room, startDateTime, endDateTime);
            schedule.getWorkout().setPrice(workout.getPrice() - (workout.getPrice() * (specialDateDAO.findDiscountForDay(schedule)/100)));

            schedules.add(schedule);
        }

        public ShoppingCart getCart() {
            {
                return new ShoppingCart(schedules);
            }
        }
    }

    @Override
    public ShoppingCart findOne(String username) {
        String sql = "select * \n" +
                "from shoppingCart sc inner join scheduales s\n" +
                "on sc.schedualeId = s.schedualeId\n" +
                "where userId =  ? ;\n";
                ShoppingCartRowCallbackHandler rowCallbackHandler = new ShoppingCartRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,username);
        return rowCallbackHandler.getCart();
    }

    @Override
    public boolean isInCart(Schedule schedule) {
        String sql = "select count(schedualeId) from shoppingcart where schedualeId = ?;\n";

        if (schedule.getWorkout().getType().equals(EWorkoutType.GRUPNI)){
            return jdbcTemplate.queryForObject(sql,Integer.class,schedule.getId()) >= schedule.getRoom().getCapacity();
        }
        return jdbcTemplate.queryForObject(sql,Integer.class,schedule.getId()) != 0;
    }

    @Override
    public boolean canAddToCart(Schedule schedule, String username) {
       String sql = "select count(*) from scheduales s \n" +
               "inner join shoppingCart r on s.schedualeId = r.schedualeId \n" +
               "where r.userId= ? and\n" +
               "((? >= s._dateTime and ? <= s.endDateTime) or( ? <= s.endDateTime and ? >= s._dateTime));";

       return jdbcTemplate.queryForObject(sql,Integer.class,username,
               Timestamp.valueOf(schedule.getDateTime()),Timestamp.valueOf(schedule.getDateTime()),
               Timestamp.valueOf(schedule.getEndDateTime()),Timestamp.valueOf(schedule.getEndDateTime())) == 0;
    }

    @Override
    public boolean save(int scheduleId, String username) {
        String sql ="insert into shoppingCart(userId ,schedualeId) values (?,?);";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setString(index++,username);
                preparedStatement.setInt(index++,scheduleId);

                return preparedStatement;
            }
        };
        try{
            jdbcTemplate.update(preparedStatementCreator);
        } catch (Exception e){
           return false;
        }
        return true;
    }

    @Override
    public boolean delete(int scheduleId, String username) {
        String sql ="delete from shoppingCart where userId = ?  and schedualeId = ? ;";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setString(index++,username);
                preparedStatement.setInt(index++,scheduleId);

                return preparedStatement;
            }
        };
       return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean clear(String username) {
        String sql ="delete from shoppingCart where userId = ?;";
        return jdbcTemplate.update(sql,username) == 1;
    }
}
