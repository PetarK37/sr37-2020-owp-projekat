package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.CommentDAO;
import com.SR37.OWP.dao.WorkoutDAO;
import com.SR37.OWP.model.Comment;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.enums.EcommentStatus;
import org.apache.tomcat.util.net.jsse.PEMFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Repository
public class CommentDAOImpl implements CommentDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WorkoutDAO workoutDAO;

    private class CommentRowCallbackHandler implements RowCallbackHandler{

        private ArrayList<Comment> comments = new ArrayList<>();
        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Long commentId = rs.getLong("commentId");
            String content = rs.getString("content");
            float rating = rs.getFloat("rating");
            LocalDateTime dateOfComment = rs.getTimestamp("_date").toLocalDateTime();
            String userId = rs.getString("userId");
            EcommentStatus status = EcommentStatus.valueOf(rs.getString("_status"));
            Workout workout = workoutDAO.findOne(rs.getInt("workoutId"));

            String username = userId;

            if (rs.getBoolean("anonymous") == true){
                username = "anonymous";
            }

            comments.add(new Comment(commentId,content,rating,dateOfComment,username,workout,status));
        }

        public ArrayList<Comment> getComments() {
            return comments;
        }
    }

    @Override
    public ArrayList<Comment> findAll(int workoutId) {
        String sql = "select * from comments where workoutId = ? and _status = 'ODOBREN';\n";
        CommentRowCallbackHandler commentRowCallbackHandler = new CommentRowCallbackHandler();
        jdbcTemplate.query(sql,commentRowCallbackHandler,workoutId);
        return commentRowCallbackHandler.getComments();
    }

    @Override
    public ArrayList<Comment> findAllCommentApplications() {
        String sql = "select * from comments where _status = 'NA_ČEKANJU';\n";
        CommentRowCallbackHandler commentRowCallbackHandler = new CommentRowCallbackHandler();
        jdbcTemplate.query(sql,commentRowCallbackHandler);
        return commentRowCallbackHandler.getComments();
    }

    @Override
    public boolean acceptComment(Comment comment) {
        String sql = "update comments  set _status = 'ODOBREN' where commentId = ?;\n";
        return jdbcTemplate.update(sql,comment.getId()) == 1 && workoutDAO.updateRating(comment.getWorkout());

    }

    @Override
    public boolean declineComment(Comment comment) {
        String sql = "update comments  set _status = 'NIJE_ODOBREN' where commentId = ?;\n";
        return jdbcTemplate.update(sql,comment.getId()) == 1;
    }

    @Override
    public boolean save(Comment comment,boolean isAnonymus) {
        String sql;
        if (isAnonymus == false){
             sql = "insert into comments(workoutId,content,rating,_date,userId,anonymous) " +
                    "values (?,?,?,?,?,false);\n";
        }else {
            sql = "insert into comments(workoutId,content,rating,_date,userId) " +
                    "values (?,?,?,?,?);\n";
        }
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
               PreparedStatement preparedStatement = con.prepareStatement(sql);

               int index = 1;
               preparedStatement.setLong(index++,comment.getWorkout().getId());
               preparedStatement.setString(index++,comment.getContent());
               preparedStatement.setFloat(index++,comment.getRating());
               preparedStatement.setTimestamp(index++, Timestamp.valueOf(comment.getDate()));
               preparedStatement.setString(index++, comment.getAuthor());
               return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean allreadyCommented(String username, int workoutId) {
        String sql = "select count(*) from comments where workoutId = ? and userId = ? and _status in ('NA_ČEKANJU','ODOBREN');\n";
         boolean test = jdbcTemplate.queryForObject(sql,Integer.class,username,workoutId) > 0;
        return jdbcTemplate.queryForObject(sql,Integer.class,workoutId,username) > 0;
    }

    @Override
    public Comment findOne(int commentId) {
        String sql = "select * from comments where commentId = ?;\n";
        CommentRowCallbackHandler commentRowCallbackHandler = new CommentRowCallbackHandler();
        jdbcTemplate.query(sql,commentRowCallbackHandler,commentId);
        return commentRowCallbackHandler.getComments().get(0);
    }
}
