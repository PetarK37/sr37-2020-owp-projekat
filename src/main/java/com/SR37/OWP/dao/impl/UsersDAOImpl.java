package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.UsersDAO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.UserSearchDTO;
import com.SR37.OWP.model.enums.EUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@Repository
public class UsersDAOImpl implements UsersDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class UsersRowCallBackHandler implements RowCallbackHandler {

        private ArrayList<User> users = new ArrayList<>();
        private int rowCount;

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            String username = rs.getString("username");
            String password = rs.getString("_password");
            String email = rs.getString("email");
            String name = rs.getString("_name");
            String surname = rs.getString("surname");
            LocalDate birthday = rs.getTimestamp("birthday").toLocalDateTime().toLocalDate();
            String address = rs.getString("addres");
            String phoneNum = rs.getString("phone_num");
            LocalDateTime dateOfReservation = rs.getTimestamp("dateTimeOfRegistration").toLocalDateTime();
            EUserRole role = EUserRole.valueOf(rs.getString("_role"));
            boolean isBlocked = rs.getBoolean("isBlocked");
            User user = new User(username,password,email,name,surname,birthday,address,phoneNum,dateOfReservation,role,isBlocked);

            users.add(user);
            rowCount ++;
        }

        public ArrayList<User> getUsers(){return users;}

        public int getRowCount() {
            return this.rowCount;
        }

    }

    @Override
    public ArrayList<User> findAll() {
        String sql = "select * from users;";

        UsersRowCallBackHandler rowCallBackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler);
        return rowCallBackHandler.getUsers();
    }

    @Override
    public ArrayList<User> findAll(UserSearchDTO userSearchDTO) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String criterium = userSearchDTO.getSortingCriterium() == "%%" ? "'%%'" :  userSearchDTO.getSortingCriterium() ;
                String order = userSearchDTO.getSort() == "%%" ? "asc" :  userSearchDTO.getSort() ;

                String sql = "select * from users where username like ? and _role like ?\n" +
                        "order by " + criterium + " " + order + " ;" ;

                PreparedStatement preparedStatement = con.prepareStatement(sql);

                String username = userSearchDTO.getUsername()== "%%" ? "%%" : "%" + userSearchDTO.getUsername() + "%";

                int index =1;
                preparedStatement.setString(index++,username);
                preparedStatement.setString(index++,userSearchDTO.getUserRole() == null ? "%%" : userSearchDTO.getUserRole().toString());

                System.out.println(preparedStatement);
                return preparedStatement;
            }
        };
        UsersRowCallBackHandler rowCallbackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(preparedStatementCreator,rowCallbackHandler);
        return rowCallbackHandler.getUsers();
    }

    @Override
    public User findOne(String username) {
        String sql = "select * from users where username = ? ;";

        UsersRowCallBackHandler rowCallBackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,username);
        return rowCallBackHandler.getUsers().get(0);
    }

    @Override
    public boolean usernameIsFree(String username) {
        String sql = "select * from users where username = ?;";

        UsersRowCallBackHandler rowCallBackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,username);
        return rowCallBackHandler.getUsers().isEmpty();
    }

    @Override
    public boolean emailIsFree(String email) {
        String sql = "select * from users where email = ?;";

        UsersRowCallBackHandler rowCallBackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,email);
        return rowCallBackHandler.getUsers().isEmpty();
    }


    public boolean findOne(String username, String email) {
        String sql = "select * from users where username = ? or email = ? ;";

        UsersRowCallBackHandler rowCallBackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,username,email);
        return rowCallBackHandler.getUsers().isEmpty();
    }

    @Override
    public int findNotBlocked(String username,String password) {
        String sql = "select * from users where username = ? and _password = ? and  isBlocked = 0;";

        UsersRowCallBackHandler rowCallBackHandler = new UsersRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,username,password);
        return rowCallBackHandler.getRowCount();
    }

    @Override
    public boolean save(User user) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration) " +
                        "values(?,?,?,?,?,?,?,?,now());\n";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setString(index++,user.getUsername());
                preparedStatement.setString(index++,user.getPassword());
                preparedStatement.setString(index++,user.getEmail());
                preparedStatement.setString(index++,user.getName());
                preparedStatement.setString(index++,user.getSurname());
                preparedStatement.setString(index++,user.getBirthday().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                preparedStatement.setString(index++,user.getAddress());
                preparedStatement.setString(index++,user.getPhoneNum());

                return preparedStatement;
            }
        };

        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean applyBlocStatus(boolean status,String username) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "update users set isBlocked = ? where username = ?;";

                PreparedStatement preparedStatement = con.prepareStatement(sql);

                int index = 1;
                preparedStatement.setBoolean(index++,status);
                preparedStatement.setString(index++,username);
                return preparedStatement;
            }
        };

        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean update(User user,String begginingUsername) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "update users " +
                        "set _name = ?, surname = ?, username = ?, email = ?, birthday = ?, " +
                        "addres = ? ,phone_num = ?, _role = ? where username = ?;";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                preparedStatement.setString(index++, user.getName());
                preparedStatement.setString(index++, user.getSurname());
                preparedStatement.setString(index++, user.getUsername());
                preparedStatement.setString(index++, user.getEmail());
                preparedStatement.setString(index++,user.getBirthday().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                preparedStatement.setString(index++, user.getAddress());
                preparedStatement.setString(index++, user.getPhoneNum());
                preparedStatement.setString(index++, user.getUserRole().toString());
                preparedStatement.setString(index++, begginingUsername);

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean update(String password, String username) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "update users " +
                        "set _password = ? where username = ? ;";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                preparedStatement.setString(index++, password);
                preparedStatement.setString(index++, username);

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

}
