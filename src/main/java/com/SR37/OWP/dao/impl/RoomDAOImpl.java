package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.RoomDAO;
import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.RoomSearchDTO;
import com.SR37.OWP.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class RoomDAOImpl implements RoomDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class RoomRowCallbackHandler implements RowCallbackHandler{
        private ArrayList<Room> rooms = new ArrayList<Room>();
        private int rowcount;

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            String id = rs.getString("roomId");
            int capacity = rs.getInt("capacity");

            rooms.add(new Room(id,capacity));
            rowcount++;
        }

        public ArrayList<Room> getRooms() {
            return rooms;
        }

        public int getRowcount() {
            return rowcount;
        }
    }

    @Override
    public ArrayList<Room> findAll() {
        String sql = "select * from \n" +
                "rooms where isDeleted = false;";
        RoomRowCallbackHandler rowCallbackHandler = new RoomRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler);
        return rowCallbackHandler.getRooms();
    }

    @Override
    public ArrayList<Room> findAll(RoomSearchDTO roomSearchDTO) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String criterium = roomSearchDTO.getSortingCriterium() == "%%" ? "'%%'" :  roomSearchDTO.getSortingCriterium() ;
                String order = roomSearchDTO.getSort() == "%%" ? "asc" :  roomSearchDTO.getSort() ;

                String sql = "select * \n" +
                        "from rooms \n" +
                        "where roomId like ?  and isDeleted = false\n" +
                        "order by " + criterium + " " + order + " ;" ;

                PreparedStatement preparedStatement = con.prepareStatement(sql);

                String id = roomSearchDTO.getId()== "%%" ? "%%" : "%" + roomSearchDTO.getId() + "%";

                int index =1;
                preparedStatement.setString(index++,id);

                System.out.println(preparedStatement);
                return preparedStatement;
            }
        };
        RoomRowCallbackHandler rowCallbackHandler = new RoomRowCallbackHandler();
        jdbcTemplate.query(preparedStatementCreator,rowCallbackHandler);
        return rowCallbackHandler.getRooms();
    }

    @Override
    public boolean roomIsFree(Room room) {
       String sql = "select * from \n" +
               "rooms r left outer join scheduales s\n" +
               "on r.roomId = s.roomId\n" +
               "where (ifNull(s.endDateTime,'2000-01-1 10:00') > now()) and r.roomId = ? ;";

       RoomRowCallbackHandler rowCallbackHandler = new RoomRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,room.getRoomID());

        return rowCallbackHandler.rowcount == 0;
    }

    @Override
    public Room finOne(String roomId) {
        String sql = "select * from \n" +
                "rooms where roomId = ? and isDeleted = false;";
        RoomRowCallbackHandler rowCallbackHandler = new RoomRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,roomId);

        if (rowCallbackHandler.getRowcount() > 0){
            return rowCallbackHandler.getRooms().get(0);
        }
        return null;
    }

    @Override
    public boolean remove(String roomId) {
        String sql =  "update rooms set isDeleted = true where roomId = ?;\n";
        return jdbcTemplate.update(sql,roomId) == 1;
    }

    @Override
    public boolean save(Room room) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "insert into rooms(roomId,capacity) values (?,?)";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setString(index++, room.getRoomID());
                preparedStatement.setInt(index++,room.getCapacity());

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean update(Room room) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "update rooms set capacity = ? where roomId = ?;";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                preparedStatement.setInt(index++,room.getCapacity());
                preparedStatement.setString(index++,room.getRoomID());

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean roomIsFree(Schedule schedule) {
        String sql = " select count(*) \n" +
                " from scheduales s  \n" +
                " where  s.roomId = ?" +
                "        and ((? >= _dateTime and ? <= endDateTime)\n" +
                "        or(? <= endDateTime and ? >= _dateTime) " +
                "or (? > endDateTime and ? < _dateTime));";



        return jdbcTemplate.queryForObject(sql,Integer.class,schedule.getRoom().getRoomID(),
                Timestamp.valueOf(schedule.getDateTime()),Timestamp.valueOf(schedule.getDateTime()),
                Timestamp.valueOf(schedule.getEndDateTime()),Timestamp.valueOf(schedule.getEndDateTime()),Timestamp.valueOf(schedule.getEndDateTime()),Timestamp.valueOf(schedule.getDateTime())) == 0;
    }

    public boolean canEditRoom(String roomId){
        String sql = "select * from reservations r inner join scheduales s on s.schedualeId = r.schedualeId \n" +
                "inner join rooms ro on s.roomId = ro.roomId where convert(s._dateTime,date) > now() and s.roomId = ?";

        RoomRowCallbackHandler rowCallbackHandler = new RoomRowCallbackHandler();

        jdbcTemplate.query(sql,rowCallbackHandler,roomId);
        return rowCallbackHandler.rowcount == 0;
    }

}
