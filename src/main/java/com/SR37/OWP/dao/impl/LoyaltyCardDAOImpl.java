package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.LoyaltyCardDAO;
import com.SR37.OWP.dao.UsersDAO;
import com.SR37.OWP.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
@Repository
public class LoyaltyCardDAOImpl implements LoyaltyCardDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UsersDAO usersDAO;

    @Override
    public Integer getMyLoyaltyPoints(String username) {
        String sql = "select points \n" +
                "from loyaltyCards l  right join users u\n" +
                " on l.userId = u.username\n" +
                " where u.username =  ?;";

        return jdbcTemplate.queryForObject(sql,Integer.class,username);
    }

    @Override
    public ArrayList<User> getAllApplicants() {
        String sql = "select userId \n" +
                "from cardApplications \n" +
                " ;";

        ArrayList<User> retVal =  new ArrayList<User>();
        for (String username : jdbcTemplate.queryForList(sql,String.class)){
            retVal.add(usersDAO.findOne(username));
        }
        return retVal;
    }

    @Override
    public boolean hasApplied(String username) {
       String sql = "select count(*) from cardApplications where userId = ? ;\n";
       return jdbcTemplate.queryForObject(sql,Integer.class,username) > 0;
    }

    @Override
    public boolean apply(String username) {
        String sql ="insert into cardApplications(userId) values (?);\n";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setString(1,username);

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean acceptApplication(String id) {
        String sql ="insert into loyaltyCards(userId) values (?);\n";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setString(1,id);

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1 && declineApplication(id);
    }

    @Override
    public boolean declineApplication(String id) {
        String sql = "delete from cardApplications where userId = ? ;";
        return jdbcTemplate.update(sql,id) == 1;
    }

    @Override
    public boolean addPoints(String username, int points) {
       String sql = "update loyaltyCards set points = points + ? where userId = ? ;\n";
       PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
           @Override

           public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
               PreparedStatement preparedStatement = con.prepareStatement(sql);
               int index = 1;
               preparedStatement.setInt(index++,points);
               preparedStatement.setString(index++,username);

               return preparedStatement;
           }
       };
        return jdbcTemplate.update(preparedStatementCreator) == 1;

    }

    @Override
    public boolean removePoints(String username, int points) {
        String sql = "update loyaltyCards set points = points - ? where userId= ? ;\n";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setInt(index++,points);
                preparedStatement.setString(index++,username);

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

}
