package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.WorkoutKindDAO;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class WorkoutKindDAOImpl implements WorkoutKindDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class WorkoutKindRowCallBackHandler implements RowCallbackHandler {
        private ArrayList<WorkoutKind> workoutKinds = new ArrayList<>();
        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Long typeId = rs.getLong("kindId");
            String typeName = rs.getString("kindName");
            String typeDescription = rs.getString("kindDescription");

            workoutKinds.add( new WorkoutKind(typeId,typeName,typeDescription));
        }

        public ArrayList<WorkoutKind> getWorkoutKinds(){return this.workoutKinds;}

    }

    @Override
    public ArrayList<WorkoutKind> findAll() {
        String sql = "select * from workoutKinds;\n";

        WorkoutKindRowCallBackHandler rowCallBackHandler = new WorkoutKindRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler);
        return rowCallBackHandler.getWorkoutKinds();
    }

    @Override
    public ArrayList<WorkoutKind> findAll(List<Integer> ids) {
        String inSql = String.join(",", Collections.nCopies(ids.size(),"?"));

        String sql = "select * from workoutKinds where kindId in (%s);\n";

        WorkoutKindRowCallBackHandler rowCallBackHandler = new WorkoutKindRowCallBackHandler();
        jdbcTemplate.query(String.format(sql,inSql),rowCallBackHandler,ids.toArray());
        return rowCallBackHandler.getWorkoutKinds();
    }

    @Override
    public int[] saveAllKindsPerWorkout(Workout workout) {
        String sql = "insert into kindPerWorkout(kindId,workoutId) values(?,?)";

        return jdbcTemplate.batchUpdate(sql,
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, String.valueOf(workout.getKinds().get(i).getId()));
                        ps.setString(2, String.valueOf(workout.getId()));
                    }

                    @Override
                    public int getBatchSize() {
                        return workout.getKinds().size();
                    }
                });
    }

    public WorkoutKind findOne(int id) {
        String sql =  "select *\n" +
                "from workoutkinds \n" +
                " where kindId = ?";

        WorkoutKindRowCallBackHandler rowCallBackHandler = new WorkoutKindRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,id);
        return rowCallBackHandler.getWorkoutKinds().get(0);
    }


}
