package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.ImageDAO;
import com.SR37.OWP.model.ImageDTO;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class ImageDAOImpl implements ImageDAO {
    @Override
    public boolean Save(ImageDTO imageDTO) {
        Path absolutePath = Paths.get("").toAbsolutePath();
        Path finalPath = Path.of((absolutePath + "\\src\\main\\resources\\" + imageDTO.getPath()));
        try {
            Files.write(finalPath,imageDTO.getBytes());
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
