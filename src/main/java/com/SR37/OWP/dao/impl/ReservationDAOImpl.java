package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.ReservationDAO;
import com.SR37.OWP.dao.ScheduleDAO;
import com.SR37.OWP.dao.UsersDAO;
import com.SR37.OWP.model.Reservation;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Repository
public class ReservationDAOImpl implements ReservationDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ScheduleDAO scheduleDAO;

    @Autowired
    private UsersDAO usersDAO;


    private class ReservationRowCallbackHandler implements RowCallbackHandler{

        private ArrayList<Reservation> reservations = new ArrayList<>();
        @Override
        public void processRow(ResultSet rs) throws SQLException {
            User user = usersDAO.findOne(rs.getString("userId"));
            Schedule schedule = scheduleDAO.findOne(rs.getLong("schedualeId"));
            LocalDateTime localDateTime = rs.getTimestamp("_date").toLocalDateTime();
            Float price = rs.getFloat("currentPrice");

            Reservation reservation = new Reservation(localDateTime,schedule,user,price);

            reservations.add(reservation);
        }
        public ArrayList<Reservation> getReservations() {
            return reservations;
        }

    }


    @Override
    public Reservation findOne(String username, Long scheduleId) {
        String sql = "select * from reservations where userId = ? and schedualeId = ? " +
                "order by _date desc;\n";
        ReservationRowCallbackHandler rowCallbackHandler = new ReservationRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,username,scheduleId);
        return rowCallbackHandler.getReservations().get(0);
    }

    @Override
    public Reservation findByWorkout(String username, Integer workoutId) {
        String sql = "select r.userId,r.schedualeId,r._date,r.currentPrice  from reservations r left join scheduales s on r.schedualeId = s.schedualeId  left join workouts w " +
                "on w.workoutId = s. workoutId where r.userId = ?  and w.workoutId = ?\n" +
                "                order by _date desc;\n";
        ReservationRowCallbackHandler rowCallbackHandler = new ReservationRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,username,workoutId);
        if (rowCallbackHandler.getReservations().size() == 0){
            return null;
        }
        return rowCallbackHandler.getReservations().get(0);
    }

    @Override
    public ArrayList<Reservation> findAllByRange(LocalDateTime startDate, LocalDateTime endDate) {
        String sql = "select * from reservations where convert(_date,date) >= convert(?,date) and  convert(_date,date) <= convert(?,date) ;";
        ReservationRowCallbackHandler rowCallbackHandler = new ReservationRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,Timestamp.valueOf(startDate),Timestamp.valueOf(endDate));
        return rowCallbackHandler.getReservations();
    }

    @Override
    public ArrayList<Reservation> findAll(String username) {
        String sql = "select * from reservations where userId = ? " +
                "order by _date desc;";

        ReservationRowCallbackHandler rowCallbackHandler = new ReservationRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,username);
        return rowCallbackHandler.getReservations();
    }

    @Override
    public boolean save(Reservation reservation) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "insert into reservations(userId,schedualeId,_date,currentPrice)  " +
                        "values(?,?,?,?);";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                preparedStatement.setString(index++,reservation.getUser().getUsername());
                preparedStatement.setLong(index++,reservation.getSchedule().getId());
                preparedStatement.setTimestamp(index++, Timestamp.valueOf(reservation.getDateTime()));
                preparedStatement.setFloat(index++,reservation.getPrice());

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1 && scheduleDAO.UpdateStatus(reservation.getSchedule());
    }

    @Override
    public boolean delete(Reservation reservation) {
        String sql = "delete from reservations where userId = ? and schedualeId = ?;";
        return jdbcTemplate.update(sql,reservation.getUser().getUsername(),reservation.getSchedule().getId()) == 1 && scheduleDAO.DeleteStatus(reservation.getSchedule());
    }

    @Override
    public boolean canReserve(Schedule schedule, String username) {
        String sql = "select count(*) from scheduales s \n" +
                "inner join reservations r on s.schedualeId = r.schedualeId \n" +
                "where r.userId= ? and\n" +
                "((? >= s._dateTime and ? <= s.endDateTime) or( ? <= s.endDateTime and ? >= s._dateTime));";

        return jdbcTemplate.queryForObject(sql,Integer.class,username,
                Timestamp.valueOf(schedule.getDateTime()),Timestamp.valueOf(schedule.getDateTime()),
                Timestamp.valueOf(schedule.getEndDateTime()),Timestamp.valueOf(schedule.getEndDateTime())) == 0;
    }
}
