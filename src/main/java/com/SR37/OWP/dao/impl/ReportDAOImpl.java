package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.ReportsDAO;
import com.SR37.OWP.dao.WorkoutDAO;
import com.SR37.OWP.model.ReportDTO;
import com.SR37.OWP.model.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Repository
public class ReportDAOImpl implements ReportsDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WorkoutDAO workoutDAO;

    @Override
    public ArrayList<ReportDTO> findAllByDates(LocalDateTime startDate, LocalDateTime endDate) {
        ArrayList<ReportDTO> retVal = new ArrayList<>();
        String priceSql = "select ifNull(sum(currentPrice),0) from reservations r left join scheduales s on r.schedualeId = s.schedualeId  where (convert(_date,date) >= convert(?,date) and  convert(_date,date) <= convert(?,date) ) and workoutId = ?; \n";
        String numSql = "select count(workoutId) from reservations r left join scheduales s on r.schedualeId = s.schedualeId  where (convert(_date,date) >= convert(?,date) and  convert(_date,date) <= convert(?,date) ) and workoutId = ?; \n";
        for (Workout w : workoutDAO.findAll()){
            if (wasReserved(w,startDate,endDate)){
                float price = jdbcTemplate.queryForObject(priceSql,Float.class,Timestamp.valueOf(startDate),Timestamp.valueOf(endDate),w.getId()) ;
                int count = jdbcTemplate.queryForObject(numSql,Integer.class,Timestamp.valueOf(startDate),Timestamp.valueOf(endDate),w.getId());
                retVal.add(new ReportDTO(w,count,price));
            }
        }
        return retVal;
    }


    private boolean wasReserved(Workout workout,LocalDateTime startDate, LocalDateTime endDate){
        String sql = "select count(*) from reservations r left join scheduales s on r.schedualeId = s.schedualeId  where (convert(_date,date) >= convert(?,date) and  convert(_date,date) <= convert(?,date) ) and s.workoutId = ?;\n";
        return jdbcTemplate.queryForObject(sql,Integer.class,Timestamp.valueOf(startDate),Timestamp.valueOf(endDate),workout.getId()) > 0;
    }
}
