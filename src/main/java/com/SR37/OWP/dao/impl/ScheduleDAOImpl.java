package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.ScheduleDAO;
import com.SR37.OWP.dao.ShoppingCartDAO;
import com.SR37.OWP.dao.WorkoutDAO;
import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.enums.EWorkoutType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Repository
public class ScheduleDAOImpl implements ScheduleDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WorkoutDAO workoutDAO;

    @Autowired
    private ShoppingCartDAO shoppingCartDAO;

    private class ShedualeRowCallbackHandler implements RowCallbackHandler{
        private ArrayList<Schedule> sheduales = new ArrayList<>();
        private int rowCount;

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Long shadualeId = rs.getLong("schedualeId");
            LocalDateTime startDateTime = rs.getTimestamp("_dateTime").toLocalDateTime();
            LocalDateTime endDateTime = rs.getTimestamp("endDateTime").toLocalDateTime();

            String roomID = rs.getString("roomId");
            int roomCapacity = rs.getInt("capacity");
            int id = rs.getInt("workoutId");


            Workout workout = workoutDAO.findOne(id);
            Room room = new Room(roomID,roomCapacity);
            Schedule schedule = new Schedule(shadualeId,workout,room,startDateTime,endDateTime);

            sheduales.add(schedule);
            rowCount++;
        }

        public ArrayList<Schedule> getScheduales(){return sheduales;}

        public int getRowCount() {
            return this.rowCount;
        }
    }
    @Override
    public ArrayList<Schedule> findAll() {
        String sql = "select * from \n" +
                "scheduales s inner join rooms r on s.roomId = r.roomId ;";


        ShedualeRowCallbackHandler rowCallbackHandler = new ShedualeRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler);
        return rowCallbackHandler.getScheduales();
    }

    @Override
    public Schedule findOne(long schedualeId) {
        String sql = "select * from \n" +
                "scheduales s inner join rooms r on s.roomId = r.roomId  where s.schedualeId = ?;";


        ShedualeRowCallbackHandler rowCallbackHandler = new ShedualeRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,schedualeId);
        return rowCallbackHandler.getScheduales().get(0);
    }

    @Override
    public ArrayList<Schedule> findAllCurrent() {
        String sql = "select * from \n" +
                "scheduales s inner join rooms r on s.roomId = r.roomId \n" +
                "where s._dateTime > now() and s.isFree = 1;";

        ShedualeRowCallbackHandler rowCallbackHandler = new ShedualeRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler);
        ArrayList<Schedule> retVal = rowCallbackHandler.getScheduales();

        return retVal;
    }

    @Override
    public ArrayList<Schedule> findAll(int workoutId) {
        String sql = "select * from \n" +
                "scheduales s inner join rooms r on s.roomId = r.roomId and workoutId = ?;";


        ShedualeRowCallbackHandler rowCallbackHandler = new ShedualeRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,workoutId);
        return rowCallbackHandler.getScheduales();
    }

    @Override
    public ArrayList<Schedule> findAllCurrent(int workoutId) {
        String sql = "select * from \n" +
                "scheduales s inner join rooms r on s.roomId = r.roomId \n" +
                "where s._dateTime > now() and s.isFree = 1 and workoutId = ?;";

        ShedualeRowCallbackHandler rowCallbackHandler = new ShedualeRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,workoutId);
        ArrayList<Schedule> retVal = rowCallbackHandler.getScheduales();
        for (int i = 0; i < retVal.size(); i++){
            if (shoppingCartDAO.isInCart(retVal.get(i))){
                retVal.remove(i);
            }
        }
        return retVal;
    }

    @Override
    public boolean Save(Schedule schedule) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "insert into scheduales(workoutId,roomId,_dateTime,endDateTime) " +
                        "values(?,?,?,?);";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                preparedStatement.setLong(index++,schedule.getWorkout().getId());
                preparedStatement.setString(index++,schedule.getRoom().getRoomID());
                preparedStatement.setTimestamp(index++, Timestamp.valueOf(schedule.getDateTime()));
                preparedStatement.setTimestamp(index++, Timestamp.valueOf(schedule.getEndDateTime()));

                return preparedStatement;
            }
        };
        return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean UpdateStatus(Schedule schedule) {
        String sql = "update scheduales set isFree = true where schedualeId = ?;";

        if (schedule.getWorkout().getType().equals(EWorkoutType.POJEDINAČNI) && (canReserve(schedule.getId()) >= 0)){
            sql = "update scheduales set isFree = false where schedualeId = ?;";
        }
        if (canReserve(schedule.getId()) >= schedule.getRoom().getCapacity() ){
            sql = "update scheduales set isFree = false where schedualeId = ?;";
        }

        return jdbcTemplate.update(sql,schedule.getId()) > 0;
    }

    @Override
    public boolean DeleteStatus(Schedule schedule) {
        String sql = "update scheduales set isFree = true where schedualeId = ?;";
        return jdbcTemplate.update(sql,schedule.getId()) > 0;
    }

    private int canReserve(long scheduleId){
        String sql = "select count(*) from reservations where schedualeId = ?;";
        return jdbcTemplate.queryForObject(sql,Integer.class,scheduleId);
    }
}
