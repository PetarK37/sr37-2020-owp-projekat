package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.SpecialDateDAO;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.SpecialDateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;


@Repository
public class SpecialDateDAOImpl implements SpecialDateDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class SpecialDateRowCallbackHandler implements RowCallbackHandler {
        private ArrayList<Float>  discount =  new ArrayList<Float>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            discount.add(rs.getFloat("discount"));
        }

        public ArrayList<Float> getDiscount() {
            {
                return discount;
            }
        }
    }

    @Override
    public Float findDiscountForDay(Schedule schedule) {
        String sql = "select count(discount), coalesce(discount,0) as discount from specialDates s right join workouts w on s.workoutId = w.workoutId where (startDate <= convert(?,date) and endDate >= convert(?,date)) and s.workoutId = ?;\n \n";
        SpecialDateRowCallbackHandler rowCallbackHandler = new SpecialDateRowCallbackHandler();
        jdbcTemplate.query(sql,rowCallbackHandler,Timestamp.valueOf(schedule.getDateTime()),Timestamp.valueOf(schedule.getDateTime()),schedule.getWorkout().getId());
        return  rowCallbackHandler.getDiscount().get(0);
    }

    @Override
    public boolean isSpecialDate(ShoppingCart shoppingCart) {
        String sql = "select count(*) from specialDates s right join workouts w on s.workoutId = w.workoutId where (startDate <= convert(?,date) and endDate >= convert(?,date)) and s.workoutId = ?;\n";
        for (Schedule s : shoppingCart.getSchedules()){
            if (jdbcTemplate.queryForObject(sql,Integer.class,Timestamp.valueOf(s.getDateTime()),Timestamp.valueOf(s.getDateTime()),s.getWorkout().getId()) != 0){
                return true;
            }

        }
        return  false;
    }

    @Override
    public boolean save(SpecialDateDTO specialDateDTO) {
        String sql ="insert into specialDates(startDate,endDate,workoutId,discount)  values (?,?,?,?);";

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;

                preparedStatement.setTimestamp(index++, Timestamp.valueOf(specialDateDTO.getStartDate()));
                preparedStatement.setTimestamp(index++, Timestamp.valueOf(specialDateDTO.getEndDate()));
                preparedStatement.setInt(index++,specialDateDTO.getWorkoutId());
                preparedStatement.setInt(index++,specialDateDTO.getDiscount());

                return preparedStatement;
            }
        };
        try{
            if(dateIsFree(specialDateDTO)){
                return jdbcTemplate.update(preparedStatementCreator) == 1;
            }
            return false;
        } catch (Exception e){
            return false;
        }
    }

    private boolean dateIsFree(SpecialDateDTO specialDateDTO){
        String sql = "select count(*)\n" +
                "from specialDates \n" +
                "where workoutId = ? and ((convert(?,date) >= startDate and convert(?,date) <= endDate)\n" +
                "   or(convert(?,date) <= endDate and convert(?,date) >= startDate)" +
                " or(convert(?,date) > endDate and convert(?,date) < startDate));";

        return jdbcTemplate.queryForObject(sql,Integer.class,specialDateDTO.getWorkoutId(),Timestamp.valueOf(specialDateDTO.getStartDate()),Timestamp.valueOf(specialDateDTO.getStartDate()),Timestamp.valueOf(specialDateDTO.getEndDate())
                ,Timestamp.valueOf(specialDateDTO.getEndDate()),Timestamp.valueOf(specialDateDTO.getEndDate()),Timestamp.valueOf(specialDateDTO.getStartDate())) == 0;
    }
}
