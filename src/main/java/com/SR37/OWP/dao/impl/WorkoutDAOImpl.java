package com.SR37.OWP.dao.impl;

import com.SR37.OWP.dao.WorkoutDAO;
import com.SR37.OWP.dao.WorkoutKindDAO;
import com.SR37.OWP.model.WorkoutKind;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutSearchDTO;
import com.SR37.OWP.model.enums.EWorkoutType;
import com.SR37.OWP.model.enums.EWorkoutLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class WorkoutDAOImpl implements WorkoutDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WorkoutKindDAO workoutKindDAO;


    private class WorkoutJoinRowCallBackHandler implements RowCallbackHandler {

        private Map<Long,Workout> workouts = new LinkedHashMap<>();
        @Override
        public void processRow(ResultSet rs) throws SQLException {

            long id = rs.getLong("workoutId");
            String name = rs.getString("_name");
            String description = rs.getString("_description");
            String coaches = rs.getString("instructors");
            String img_url = rs.getString("img_url");
            float price = rs.getFloat("price");
            EWorkoutType kind = EWorkoutType.valueOf(rs.getString("_type"));
            EWorkoutLevel level = EWorkoutLevel.valueOf(rs.getString("_level"));
            float rating = rs.getFloat("avgRating");

            Workout workout = workouts.get(id);

            if (workout == null){
                workout = new Workout(id,name,coaches,description,img_url,price,kind,level,rating);
                workouts.put(workout.getId(), workout);
            }

            Long typeId = rs.getLong("kindId");
            String typeName = rs.getString("kindName");
            String typeDescription = rs.getString("kindDescription");
            WorkoutKind workoutKind = new WorkoutKind(typeId,typeName,typeDescription);

            workout.getKinds().add(workoutKind);
        }
        public ArrayList<Workout> getWorkouts(){return new ArrayList<>(workouts.values()); }
    }





    @Override
    public Workout findOne(int id) {
        String sql =  "select *\n" +
                "from workouts w inner join kindperworkout kp\n" +
                "on w.workoutId = kp.workoutId \n" +
                "inner join workoutkinds k\n" +
                "on k.kindId = kp.kindId where w.workoutId = ?";

        WorkoutJoinRowCallBackHandler rowCallBackHandler = new WorkoutJoinRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler,id);
        return rowCallBackHandler.getWorkouts().get(0);
    }

    @Override
    public ArrayList<Workout> findAll() {

        String sql = "select *\n" +
                "from workouts w inner join kindperworkout kp\n" +
                "on w.workoutId = kp.workoutId \n" +
                "inner join workoutkinds k\n" +
                "on k.kindId = kp.kindId";

        WorkoutJoinRowCallBackHandler rowCallBackHandler = new WorkoutJoinRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler);
        return rowCallBackHandler.getWorkouts();
    }


    @Override
    public ArrayList<Workout> find(WorkoutSearchDTO searchDTO) {

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator(){
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "select *\n" +
                        "from workouts w inner join kindperworkout kp\n" +
                        "on w.workoutId = kp.workoutId \n" +
                        "inner join workoutkinds k\n" +
                        "on k.kindId = kp.kindId\n" +
                        "where w._name like ? and w._type like ? and w._level like ? and w.instructors like ? \n" +
                        "and k.kindName like ? and w.price >= ? and w.price <= ? "+
                        "order by "+ searchDTO.getSortingCriterium() +" " + searchDTO.getSort() + ";";

                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                String name = searchDTO.getName()== "%%" ? "%%" : "%" + searchDTO.getName() + "%";
                String instructors = searchDTO.getInstructors()== "%%" ? "%%" : "%" + searchDTO.getInstructors() + "%";

                preparedStatement.setString(index++, name);
                preparedStatement.setString(index++, searchDTO.getType());
                preparedStatement.setString(index++, searchDTO.getLevel());
                preparedStatement.setString(index++, instructors);
                preparedStatement.setString(index++, searchDTO.getKindName());
                preparedStatement.setFloat(index++, searchDTO.getPriceFrom());
                preparedStatement.setFloat(index++, searchDTO.getPriceTo());



                return preparedStatement;

            }
        };

        WorkoutJoinRowCallBackHandler rowCallBackHandler = new WorkoutJoinRowCallBackHandler();
        jdbcTemplate.query(preparedStatementCreator,rowCallBackHandler);
        return rowCallBackHandler.getWorkouts();
    }

    @Override
    public long save(Workout workout) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override

            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "insert into workouts(_name,_description,img_url,instructors,price,_type,_level) " +
                        "values(?,?,?,?,?,?,?);\n";

                PreparedStatement preparedStatement = con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
                int index = 1;

                preparedStatement.setString(index++, workout.getName());
                preparedStatement.setString(index++, workout.getDescription());
                preparedStatement.setString(index++, workout.getImgUrl());
                preparedStatement.setString(index++, workout.getCoaches());
                preparedStatement.setFloat(index++, workout.getPrice());
                preparedStatement.setString(index++, workout.getType().toString());
                preparedStatement.setString(index++, workout.getLevel().toString());

                return preparedStatement;
            }
        };
        jdbcTemplate.update(preparedStatementCreator,keyHolder);


        return keyHolder.getKey().longValue();
    }

    @Override
    public boolean update(Workout workout) {
        deleteOldKinds(workout);
        workoutKindDAO.saveAllKindsPerWorkout(workout);

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator(){
            String sql = "update workouts \n" +
                    "set _name = ?, _description = ?, instructors = ?, img_url = ?, price = ?, _type = ?,_level = ?\n" +
                    "where workoutId = ?;";
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                int index = 1;
                preparedStatement.setString(index++, workout.getName());
                preparedStatement.setString(index++, workout.getDescription());
                preparedStatement.setString(index++, workout.getCoaches());
                preparedStatement.setString(index++, workout.getImgUrl());
                preparedStatement.setFloat(index++, workout.getPrice());
                preparedStatement.setString(index++, workout.getType().toString());
                preparedStatement.setString(index++, workout.getLevel().toString());
                preparedStatement.setLong(index++, workout.getId());

                return preparedStatement;
            }
        };
       return jdbcTemplate.update(preparedStatementCreator) == 1;
    }

    @Override
    public boolean updateRating(Workout workout) {
       String sql = "update workouts  set avgRating = ? \n" +
               "where workoutId = ?;";

       return jdbcTemplate.update(sql,getAvgPrice(workout.getId()),workout.getId()) == 1;
    }

    private float getAvgPrice(long workoutId){
        String sql = "select  ifNull(Avg(rating),0) from workouts w left join comments c on w.workoutId = c.workoutId where c._status = 'ODOBREN'  group by w.workoutId having workoutId  = ?;\n";
       try {
           return jdbcTemplate.queryForObject(sql, Integer.class,workoutId);
       }catch (Exception e){
           return 0;
       }
    }

    private boolean deleteOldKinds(Workout workout){
        String Sql = "delete from kindPerWorkout where workoutId = ? ;" ;
       return jdbcTemplate.update(Sql,workout.getId()) > 0;
    }




}
