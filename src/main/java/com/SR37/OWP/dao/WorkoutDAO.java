package com.SR37.OWP.dao;

import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutSearchDTO;

import java.util.ArrayList;

public interface WorkoutDAO {

    public Workout findOne(int id);
    public ArrayList<Workout> findAll();
    public ArrayList<Workout> find(WorkoutSearchDTO searchDTO);
    public long save(Workout workout);
    public boolean update(Workout workout);
    public boolean updateRating(Workout workout);
}
