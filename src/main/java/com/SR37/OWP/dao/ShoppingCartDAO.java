package com.SR37.OWP.dao;

import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;

public interface ShoppingCartDAO {
    public ShoppingCart findOne(String username);
    public boolean isInCart(Schedule schedule);
    public boolean canAddToCart(Schedule schedule,String username);
    public boolean save(int scheduleId,String username);
    public boolean delete(int scheduleId,String username);
    public boolean clear(String username);
}
