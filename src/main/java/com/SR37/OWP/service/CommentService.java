package com.SR37.OWP.service;

import com.SR37.OWP.model.Comment;

import java.util.ArrayList;

public interface CommentService {
    public ArrayList<Comment> findAll(int workoutId);
    public ArrayList<Comment> findAllCommentApplications();
    public boolean acceptComment(Comment comment);
    public boolean declineComment(Comment comment);
    public boolean save(Comment comment,boolean anonymous);
    public  boolean allreadyCommented(String username, int workoutId);
    public Comment findOne(int commentId);
}
