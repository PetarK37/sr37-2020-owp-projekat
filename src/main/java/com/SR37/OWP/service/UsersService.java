package com.SR37.OWP.service;

import com.SR37.OWP.model.User;
import com.SR37.OWP.model.UserSearchDTO;

import java.util.ArrayList;

public interface UsersService {

    public ArrayList<User> findAll();
    public User findOne(String username);
    public ArrayList<User> findAll(UserSearchDTO userSearchDTO);
    public boolean applyBlocStatus(boolean status,String username);
    public boolean update(User user,String begginingUsername,String begginingEmail);
    public boolean updatePassword(String password,String username);

}
