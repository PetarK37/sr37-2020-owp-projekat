package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.RoomDAO;
import com.SR37.OWP.dao.ScheduleDAO;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private ScheduleDAO scheduleDAO;

    @Autowired
    private RoomDAO roomDAO;

    @Override
    public ArrayList<Schedule> findAll() {
        return scheduleDAO.findAll();
    }

    @Override
    public ArrayList<Schedule> findAllCurrent() {
        return scheduleDAO.findAllCurrent();
    }

    @Override
    public Schedule findOne(int scheduleId) {
        return scheduleDAO.findOne(scheduleId);
    }

    public ArrayList<Schedule> findAll(int workoutId){return scheduleDAO.findAll(workoutId);};
    public ArrayList<Schedule> findAllCurrent(int workoutId){return scheduleDAO.findAllCurrent(workoutId);}

    @Override
    public boolean Save(Schedule schedule) {
        if (roomDAO.roomIsFree(schedule)){
            return scheduleDAO.Save(schedule);
        }
        return false;
    }

    ;
}
