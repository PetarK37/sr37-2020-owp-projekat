package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.LoyaltyCardDAO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.service.LoyaltyCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class LoyaltyCardServiceImpl  implements LoyaltyCardService {

    @Autowired
    private LoyaltyCardDAO loyaltyCardDAO;

    @Override
    public Integer getMyLoyaltyPoints(String username) {
        return loyaltyCardDAO.getMyLoyaltyPoints(username);
    }

    @Override
    public ArrayList<User> getAllApplicants() {
        return loyaltyCardDAO.getAllApplicants();
    }

    @Override
    public boolean hasApplied(String username) {
        return loyaltyCardDAO.hasApplied(username);
    }

    @Override
    public boolean apply(String username) {
        return loyaltyCardDAO.apply(username);
    }

    @Override
    public boolean acceptApplication(String id) {
        return loyaltyCardDAO.acceptApplication(id);
    }

    @Override
    public boolean declineApplication(String id){
        return  loyaltyCardDAO.declineApplication(id);
    }

    @Override
    public boolean addPoints(String username, int points) {
        return loyaltyCardDAO.addPoints(username, points);
    }

    @Override
    public boolean removePoints(String username, int points) {
        return loyaltyCardDAO.removePoints(username, points);
    }

}
