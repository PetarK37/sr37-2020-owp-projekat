package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.CommentDAO;
import com.SR37.OWP.model.Comment;
import com.SR37.OWP.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CommentServiceImpl  implements CommentService {
    @Autowired
    private CommentDAO commentDAO;

    @Override
    public ArrayList<Comment> findAll(int workoutId) {
        return commentDAO.findAll(workoutId);
    }

    @Override
    public ArrayList<Comment> findAllCommentApplications() {
        return commentDAO.findAllCommentApplications();
    }

    @Override
    public boolean acceptComment(Comment comment) {
        return commentDAO.acceptComment(comment);
    }

    @Override
    public boolean declineComment(Comment comment) {
        return commentDAO.declineComment(comment);
    }

    @Override
    public boolean save(Comment comment,boolean anonymous) {
        return commentDAO.save(comment,anonymous);
    }

    @Override
    public boolean allreadyCommented(String username, int workoutId) {
        return commentDAO.allreadyCommented(username, workoutId);
    }

    @Override
    public Comment findOne(int commentId) {
        return commentDAO.findOne(commentId);
    }

}
