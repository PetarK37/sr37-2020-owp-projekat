package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.SpecialDateDAO;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.SpecialDateDTO;
import com.SR37.OWP.service.SpecialDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class SpecialDateServiceImpl implements SpecialDateService {

    @Autowired
    private SpecialDateDAO specialDateDAO;



    @Override
    public Float findDiscountForDay(Schedule schedule) {
        return specialDateDAO.findDiscountForDay(schedule);
    }

    @Override
    public boolean isSpecialDate(ShoppingCart shoppingCart) {
        return specialDateDAO.isSpecialDate(shoppingCart);
    }

    @Override
    public boolean save(SpecialDateDTO specialDateDTO) {
        return specialDateDAO.save(specialDateDTO);
    }
}
