package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.WorkoutKindDAO;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutKind;
import com.SR37.OWP.service.WorkoutKindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class WorkoutKindServiceImpl implements WorkoutKindService {

   @Autowired
   private WorkoutKindDAO workoutKindDAO;
    @Override
    public ArrayList<WorkoutKind> findAll() {
        return workoutKindDAO.findAll();
    }

    @Override
    public ArrayList<WorkoutKind> findAll(List<Integer> ids) {
        return workoutKindDAO.findAll(ids);
    }

    @Override
    public boolean saveAllKindsPerWorkout(Workout workout) {
        if(workoutKindDAO.saveAllKindsPerWorkout(workout).length == 0){
            return false;
        }
            return true;
    }

    @Override
    public WorkoutKind findOne(int id) {
        return workoutKindDAO.findOne(id);
    }
}
