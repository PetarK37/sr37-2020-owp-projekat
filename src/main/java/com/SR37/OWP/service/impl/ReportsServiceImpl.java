package com.SR37.OWP.service.impl;


import com.SR37.OWP.dao.ReportsDAO;
import com.SR37.OWP.model.ReportDTO;
import com.SR37.OWP.service.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportsServiceImpl implements ReportsService {
    @Autowired
    private ReportsDAO reportsDAO;

    @Override
    public ArrayList<ReportDTO> findAllByDates(LocalDateTime startDate, LocalDateTime endDate) {
        return reportsDAO.findAllByDates(startDate, endDate);
    }
}
