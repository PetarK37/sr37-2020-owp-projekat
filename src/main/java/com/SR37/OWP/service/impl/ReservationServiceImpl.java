package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.ReservationDAO;
import com.SR37.OWP.model.Reservation;
import com.SR37.OWP.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {
    @Autowired
    private ReservationDAO reservationDAO;
    @Override
    public boolean save(Reservation reservation) {
        return reservationDAO.save(reservation);
    }

    @Override
    public Reservation findOne(String username, Long scheduleId) {
        return reservationDAO.findOne(username, scheduleId);
    }

    @Override
    public ArrayList<Reservation> findAll(String username) {
        return reservationDAO.findAll(username);
    }

    @Override
    public boolean delete(Reservation reservation) {
        return reservationDAO.delete(reservation);
    }

    @Override
    public Reservation findByWorkout(String username, Integer workoutId) {
        return reservationDAO.findByWorkout(username, workoutId);
    }

    @Override
    public ArrayList<Reservation> findAllByRange(LocalDateTime startDate, LocalDateTime endDate) {
        return reservationDAO.findAllByRange(startDate, endDate);
    }
}
