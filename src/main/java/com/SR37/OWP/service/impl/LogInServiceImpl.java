package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.UsersDAO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.service.LoginAndRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogInServiceImpl implements LoginAndRegisterService {
    @Autowired
    private UsersDAO usersDAO;

    @Override
    public boolean logIn(String username, String password) {
        if (usersDAO.findNotBlocked(username,password) == 1){
            return true;
        }
        else return false;
    }

    @Override
    public boolean register(User user) {
        if (usersDAO.findOne(user.getUsername(), user.getEmail())){
            usersDAO.save(user);
            return true;
        }
        return false;
    }


}
