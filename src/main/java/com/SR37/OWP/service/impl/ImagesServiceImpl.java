package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.ImageDAO;
import com.SR37.OWP.model.ImageDTO;
import com.SR37.OWP.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImagesServiceImpl implements ImageService {

    @Autowired
    private ImageDAO imageDAO;

    @Override
    public boolean Save(ImageDTO imageDTO) {
        return imageDAO.Save(imageDTO);
    }
}
