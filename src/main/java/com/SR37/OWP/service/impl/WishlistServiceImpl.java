package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.WishListDAO;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class WishlistServiceImpl  implements WishlistService {
    @Autowired
    private WishListDAO wishListDAO;

    @Override
    public ArrayList<Workout> loadWishedWorkouts(String username) {
        return wishListDAO.loadWishedWorkouts(username);
    }

    @Override
    public boolean addToWishlist(int workoutId, String username) {
        return wishListDAO.addToWishlist(workoutId,username);
    }

    @Override
    public boolean removeFromWishlist(int workoutId, String username) {
        return wishListDAO.remove(workoutId,username);
    }
}
