package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.WorkoutDAO;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutSearchDTO;
import com.SR37.OWP.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkoutServiceImpl implements WorkoutService {

   @Autowired
   private WorkoutDAO workoutDAO;

    public Workout findOne(int id){
        return workoutDAO.findOne(id);
    }

    @Override
    public boolean update(Workout workout) {
        return workoutDAO.update(workout);
    }

    ;


    @Override
    public ArrayList<Workout> findAll() {
        return workoutDAO.findAll();
    }

    @Override
    public ArrayList<Workout> find(WorkoutSearchDTO searchDTO) {
        return workoutDAO.find(searchDTO);
    }

    @Override
    public long save(Workout workout) {
        return workoutDAO.save(workout);
    }


}
