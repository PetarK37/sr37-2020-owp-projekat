package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.UsersDAO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.UserSearchDTO;
import com.SR37.OWP.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    private UsersDAO usersDAO;

    @Override
    public ArrayList<User> findAll() {
        return usersDAO.findAll();
    }

    @Override
    public User findOne(String username) {
        return usersDAO.findOne(username);
    }

    @Override
    public ArrayList<User> findAll(UserSearchDTO userSearchDTO) {
        return usersDAO.findAll(userSearchDTO);
    }

    @Override
    public boolean applyBlocStatus(boolean status, String username) {
        return usersDAO.applyBlocStatus(status,username);
    }
    @Override
    public boolean update(User user,String begginingUsername,String begginingEmail) {

        if (!user.getUsername().equals(begginingUsername)){
            if (!usersDAO.usernameIsFree(user.getUsername())){
                    return false;
            }
        }
        if (!user.getEmail().equals(begginingEmail)){
            if (!usersDAO.emailIsFree(user.getEmail())){
                return false;
            }
        }
            return usersDAO.update(user,begginingUsername);
    }


    @Override
    public boolean updatePassword(String password, String username) {
        return usersDAO.update(password,username);
    }
}
