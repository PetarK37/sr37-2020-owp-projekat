package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.RoomDAO;
import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.RoomSearchDTO;
import com.SR37.OWP.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;

@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private RoomDAO roomDAO;

    @Override
    public ArrayList<Room> findAll() {
        return roomDAO.findAll();
    }

    @Override
    public boolean roomIsFree(Room room) {
        return roomDAO.roomIsFree(room);
    }

    @Override
    public Room finOne(String roomId) {
        return roomDAO.finOne(roomId);
    }

    @Override
    public ArrayList<Room> findAll(RoomSearchDTO roomSearchDTO) {
        return roomDAO.findAll(roomSearchDTO);
    }

    @Override
    public boolean remove(String roomId) {
        if (roomDAO.roomIsFree(new Room(roomId,1))){
            return roomDAO.remove(roomId);
        }
        return false;
    }

    @Override
    public boolean save(Room room) {
        if (roomDAO.finOne(room.getRoomID()) != null){
            return false;
        }
        return roomDAO.save(room);
    }

    @Override
    public boolean update(Room room) {
        return roomDAO.update(room);
    }

    @Override
    public boolean canEditRoom(String roomId) {
        return roomDAO.canEditRoom(roomId);
    }
}
