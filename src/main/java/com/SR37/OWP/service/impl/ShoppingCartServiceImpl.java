package com.SR37.OWP.service.impl;

import com.SR37.OWP.dao.ReservationDAO;
import com.SR37.OWP.dao.ShoppingCartDAO;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartDAO shoppingCartDAO;

    @Autowired
    private ReservationDAO reservationDAO;

    @Override
    public ShoppingCart findOne(String username) {
        return shoppingCartDAO.findOne(username);
    }

    @Override
    public boolean isInCart(Schedule schedule) {
        return shoppingCartDAO.isInCart(schedule);
    }

    @Override
    public boolean addToChart(Schedule schedule, String username) {
        if (shoppingCartDAO.canAddToCart(schedule, username) && reservationDAO.canReserve(schedule, username)) {
            return shoppingCartDAO.save((int) schedule.getId(),username);
        }
        return false;
    }

    @Override
    public boolean removeFromChart(int scheduleId, String username) {
        return shoppingCartDAO.delete(scheduleId,username);
    }

    @Override
    public boolean clear(String username) {
        return shoppingCartDAO.clear(username);
    }
}
