package com.SR37.OWP.service;

import com.SR37.OWP.model.ReportDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public interface ReportsService {
    public ArrayList<ReportDTO> findAllByDates(LocalDateTime startDate, LocalDateTime endDate);
}
