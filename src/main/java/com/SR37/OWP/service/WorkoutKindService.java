package com.SR37.OWP.service;

import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutKind;

import java.util.ArrayList;
import java.util.List;

public interface WorkoutKindService {
    public ArrayList<WorkoutKind> findAll();
    public ArrayList<WorkoutKind> findAll(List<Integer> ids);
    public boolean saveAllKindsPerWorkout(Workout workout);
    public WorkoutKind findOne(int id);
}
