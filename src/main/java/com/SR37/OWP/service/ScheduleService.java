package com.SR37.OWP.service;

import com.SR37.OWP.model.Schedule;

import java.util.ArrayList;

public interface ScheduleService {
    public ArrayList<Schedule> findAll();
    public ArrayList<Schedule> findAllCurrent();
    public Schedule findOne(int scheduleId);
    public ArrayList<Schedule> findAll(int workoutId);
    public ArrayList<Schedule> findAllCurrent(int workoutId);
    public boolean Save(Schedule schedule);
}
