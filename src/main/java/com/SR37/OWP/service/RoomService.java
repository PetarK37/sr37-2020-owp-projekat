package com.SR37.OWP.service;

import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.RoomSearchDTO;

import java.util.ArrayList;
import java.util.LinkedList;

public interface RoomService {
    public ArrayList<Room> findAll();
    public boolean roomIsFree(Room room);
    public Room finOne(String roomId);
    public ArrayList<Room> findAll(RoomSearchDTO roomSearchDTO);
    public boolean remove(String roomId);
    public boolean save(Room room);
    public boolean update(Room room);
    public boolean canEditRoom(String roomId);

}
