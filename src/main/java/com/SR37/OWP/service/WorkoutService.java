package com.SR37.OWP.service;

import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutSearchDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public interface WorkoutService {
    public ArrayList<Workout> findAll();
    public ArrayList<Workout> find(WorkoutSearchDTO searchDTO);
    public long save(Workout workout);
    public Workout findOne(int id);
    public boolean update(Workout workout);

}
