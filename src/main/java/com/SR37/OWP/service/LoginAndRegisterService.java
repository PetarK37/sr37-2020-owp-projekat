package com.SR37.OWP.service;

import com.SR37.OWP.model.User;

public interface LoginAndRegisterService {

    public boolean logIn(String username,String password);
    public boolean register(User user);
}
