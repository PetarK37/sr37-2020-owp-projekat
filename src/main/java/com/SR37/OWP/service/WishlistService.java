package com.SR37.OWP.service;

import com.SR37.OWP.model.Workout;

import java.util.ArrayList;

public interface WishlistService {
    public ArrayList<Workout> loadWishedWorkouts(String username);
    public boolean addToWishlist(int workoutId,String username);
    public boolean removeFromWishlist(int workoutId,String username);
}
