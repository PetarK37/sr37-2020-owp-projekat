package com.SR37.OWP.service;

import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.SpecialDateDTO;

import java.time.LocalDate;

public interface SpecialDateService {
    public Float findDiscountForDay(Schedule schedule);
    public boolean isSpecialDate(ShoppingCart shoppingCart);
    public boolean save(SpecialDateDTO specialDateDTO);

}
