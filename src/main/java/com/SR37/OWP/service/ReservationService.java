package com.SR37.OWP.service;

import com.SR37.OWP.model.Reservation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public interface ReservationService {
    public boolean save(Reservation reservation);
    public Reservation findOne(String username, Long scheduleId);
    public ArrayList<Reservation> findAll(String username);
    public boolean delete(Reservation reservation);
    public Reservation findByWorkout(String username,Integer workoutId);
    public ArrayList<Reservation> findAllByRange(LocalDateTime startDate, LocalDateTime endDate);

}
