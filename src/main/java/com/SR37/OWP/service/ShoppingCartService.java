package com.SR37.OWP.service;

import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;

public interface ShoppingCartService {
    public ShoppingCart findOne(String username);
    public boolean isInCart(Schedule schedule);
    public boolean addToChart(Schedule schedule,String username);
    public boolean removeFromChart(int scheduleId,String username);
    public boolean clear(String username);
}
