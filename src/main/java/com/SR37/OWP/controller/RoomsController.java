package com.SR37.OWP.controller;

import com.SR37.OWP.model.Room;
import com.SR37.OWP.model.RoomSearchDTO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

@Controller
@RequestMapping("/Rooms")
public class RoomsController {
    @Autowired
    private ServletContext servletContext;

    private String bURL;

    @Autowired
    private RoomService roomService;

    @Autowired
    private LocaleResolver localeResolver;

    @PostConstruct
    private void init(){this.bURL = servletContext.getContextPath() + "/";}

    @GetMapping
    public ModelAndView allRooms(Boolean hasError, String errorTxt, HttpSession session, HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");
        }

        ModelAndView resault = new ModelAndView("roomsPage");
        resault.addObject("error",hasError);
        resault.addObject("errorTxt",errorTxt);
        resault.addObject("rooms", roomService.findAll());
        return resault;
    }

    @GetMapping("/Search")
    public ModelAndView search(@RequestParam String roomId,
                               @RequestParam String order, @RequestParam String orderCriterium,
                               HttpSession session){

        ModelAndView resault = new ModelAndView("roomsPage");
        resault.addObject("error",false);
        resault.addObject("errorTxt","");
        resault.addObject("rooms", roomService.findAll(new RoomSearchDTO(roomId == "" ?"%%": roomId
                ,orderCriterium  == "" ? "%%": orderCriterium,order == "" ? "%%" : order)));
        return resault;
    }

    @PostMapping("/Delete")
    public ModelAndView delete(@RequestParam String id, HttpServletResponse response,HttpSession session,HttpServletRequest request) throws IOException {

        if(roomService.remove(id)){
            response.sendRedirect(bURL +"Rooms");
        }
        if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
            return allRooms(true,"You can't delete room that has appointmants in future!",session,response);

        }
        return allRooms(true,"Ne mozes da obrises sobu koja je zakzana unapred!",session,response);

    }

    @GetMapping("AddRoom")
    public ModelAndView addRoom(Boolean error,String errorTxt,HttpServletResponse response,HttpSession session) throws IOException {

        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");
        }

        ModelAndView resault = new ModelAndView("addNewRoomPage");
        resault.addObject("error",error);
        resault.addObject("errorTxt",errorTxt);
        return resault;
    }

    @PostMapping("AddRoom")
    public ModelAndView addRoomPost(HttpServletRequest request,@RequestParam String id, @RequestParam int capacity, HttpServletResponse response,HttpSession session) throws IOException {
        Room newRoom = new Room(id.replaceAll("\\s",""),capacity);
        if (roomService.save(newRoom)){
            response.sendRedirect(bURL + "Rooms");
        }
        if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
            return addRoom(true,"Room with that number already exists!",response,session);
        }
        return addRoom(true,"Sala sa tim brojem vec postoji!",response,session);
    }

    @GetMapping("EditRoom")
    public ModelAndView editRoom(@RequestParam String id,Boolean error,String errorTxt,HttpServletResponse response,HttpSession session) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");
        }

        ModelAndView resault = new ModelAndView("editRoomPage");
        resault.addObject("room",roomService.finOne(id));
        resault.addObject("error",error);
        resault.addObject("errorTxt",errorTxt);
        if (!roomService.canEditRoom(id)){
            resault.addObject("error",true);
            resault.addObject("errorTxt","You cant edit room that has appointments in future!");
        }
        return resault;
    }

    @PostMapping("EditRoom")
    public ModelAndView editRoomPost(@RequestParam String id, @RequestParam int capacity, HttpServletResponse response,HttpSession session) throws IOException {
        Room newRoom = roomService.finOne(id);
        newRoom.setCapacity(capacity);
        if (roomService.update(newRoom)){
            response.sendRedirect(bURL + "Rooms");
        }
        return addRoom(true,"There was problem with editig room in database",response,session);
    }


}
