package com.SR37.OWP.controller;

import com.SR37.OWP.model.ReportDTO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.ReportsService;
import com.SR37.OWP.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("Reports")
public class ReportsController {
    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @Autowired
    private ReportsService reportsService;

    @PostConstruct
    public void init(){bURL = servletContext.getContextPath() + "/";}

    @GetMapping()
    public String reportsPage(HttpSession session, HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return "login";
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            return "allWorkoutsPage";
        }
        return "reportsPage";
    }
    @GetMapping("GetReports")
    @ResponseBody
    public Map<String,Object> getReports (@RequestParam String dateFrom, @RequestParam String dateTo){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime startDate = LocalDate.parse(dateFrom,formatter).atStartOfDay();
        LocalDateTime endtDate = LocalDate.parse(dateTo,formatter).atTime(11,59);
         Map<String,Object> response = new LinkedHashMap<>();

         if (reportsService.findAllByDates(startDate,endtDate).isEmpty()){
             response.put("status","error");
            return response;
         }
         response.put("status","ok");
         response.put("reports",reportsService.findAllByDates(startDate,endtDate));
        return response;
    }
}
