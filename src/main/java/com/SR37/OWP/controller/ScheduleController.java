package com.SR37.OWP.controller;

import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.RoomService;
import com.SR37.OWP.service.ScheduleService;
import com.SR37.OWP.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Locale;

@Controller
@RequestMapping("Schedules")
public class ScheduleController {

    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private RoomService roomService;

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private LocaleResolver localeResolver;
    @PostConstruct
    private void init(){this.bURL= servletContext.getContextPath() + "/";}

    @GetMapping("AddSchedule")
    public ModelAndView addSchedule(Boolean error, String errorText, HttpSession session, HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        else if(!((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN))
        {
            response.sendRedirect(bURL + "Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");

        }

        ModelAndView resault =  new ModelAndView("addSchedulePage");
        resault.addObject("error",error);
        resault.addObject("errorTxt", errorText);
        resault.addObject("rooms",roomService.findAll());
        resault.addObject("workouts",workoutService.findAll());
        return  resault;
    }

    @PostMapping("AddSchedule")
    public ModelAndView addSchedulePost(HttpSession session, HttpServletResponse response, HttpServletRequest request,
                                        @RequestParam int workoutId, @RequestParam String roomId,
                                        @RequestParam String dateTimeStart, @RequestParam int duration) throws IOException {

        Schedule newSchedule = new Schedule(workoutService.findOne(workoutId), roomService.finOne(roomId),
                LocalDateTime.parse(dateTimeStart),LocalDateTime.parse(dateTimeStart).plusMinutes(duration));

        if (scheduleService.Save(newSchedule)){
            response.sendRedirect(bURL + "Workouts/AdminPage");
            return null;
        }
        if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
            return  addSchedule(true,"That room is taken, please choose another time or date!",session,response);
        }
        return  addSchedule(true,"Sala je zauzeta molim vaz izaberite drugi termin!",session,response);

    }

}
