package com.SR37.OWP.controller;


import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class ImagesController {

    @GetMapping(value="/img/{imgUrl}",
    produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getPicture(@PathVariable String imgUrl) throws IOException {
        File img = new File("E:\\Faks\\Semestar 3\\Projketi\\OWP\\Projekat SR37\\src\\main\\resources\\img\\"+imgUrl);
        InputStream inputStream = new FileInputStream(img);
        return IOUtils.toByteArray(inputStream);
    }
}
