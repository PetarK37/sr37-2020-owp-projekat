package com.SR37.OWP.controller;

import com.SR37.OWP.model.User;
import com.SR37.OWP.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("Reservations")
public class ReservationController {

    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @Autowired
    private ReservationService reservationService;

    @PostConstruct
    private void init(){this.bURL = servletContext.getContextPath() + "/";}

   @GetMapping("About")
    public ModelAndView detailsPAge(HttpSession session, HttpServletResponse response, @RequestParam String username, @RequestParam Long id) throws IOException {
       if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
           response.sendRedirect(bURL);
           return new ModelAndView("login");
       }

        ModelAndView resault =  new ModelAndView("aboutReservationPage");
        resault.addObject("role",((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().toString());
        resault.addObject("reservation", reservationService.findOne(username,id));
        return resault;
    }

    @PostMapping("CancelReservation")
    public void cacel(HttpServletResponse response,@RequestParam String username, @RequestParam Long id) throws IOException {
        if (reservationService.delete(reservationService.findOne(username,id)));{
            response.sendRedirect(bURL + "Users/About");
        }
    }
}
