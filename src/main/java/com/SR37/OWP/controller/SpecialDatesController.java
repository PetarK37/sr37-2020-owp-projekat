package com.SR37.OWP.controller;

import com.SR37.OWP.model.SpecialDateDTO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Controller
@RequestMapping("SpecialDates")
public class SpecialDatesController {
    @Autowired
    private ServletContext servletContext;
    @Autowired
    private SpecialDateService specialDateService;

    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private WorkoutService workoutService;


    private String bURL;

    @PostConstruct
    public void init(){bURL = servletContext.getContextPath() + "/";}

    @GetMapping("AddDate")
    public ModelAndView addDate(Boolean hasError, String errorTxt, HttpSession session, HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");
        }

        ModelAndView resault = new ModelAndView("addSpecialDatePage");
        resault.addObject("error",hasError);
        resault.addObject("errorTxt",errorTxt);
        resault.addObject("workouts",workoutService.findAll());
        return resault;
    }

    @PostMapping("AddDate")
    public ModelAndView addDatePost(@RequestParam ArrayList<Integer> workoutId, @RequestParam ArrayList<Integer> discount,
                                    HttpSession session, HttpServletRequest request, @RequestParam String dateTimeStart, @RequestParam String dateTimeEnd, HttpServletResponse response) throws IOException {

        ArrayList<SpecialDateDTO> dates = new ArrayList<>();
         boolean hasError = false;
        ArrayList<Integer> filteredArray = discount;
        filteredArray.removeAll(Collections.singleton(0));

        for (int i = 0; i < workoutId.size(); i++){
            dates.add(new SpecialDateDTO(LocalDate.parse(dateTimeStart),LocalDate.parse(dateTimeEnd),workoutId.get(i),filteredArray.get(i)));
        }

        for (SpecialDateDTO date : dates){
            if (specialDateService.save(date)){
                hasError = false;
            }else {
                hasError = true;
            }
        }

        if (hasError == true){
            if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
                return addDate(true,"That date is allready set for some of workouts!",session,response);
            }
            return addDate(true,"Za pojedine treninge datum vec postoji!",session,response);

        }
        response.sendRedirect(bURL +"Workouts/AdminPage");
        return new ModelAndView("allWorkoutsPage");
    }
}
