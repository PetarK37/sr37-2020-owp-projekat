package com.SR37.OWP.controller;

import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.LoyaltyCardService;
import com.SR37.OWP.service.WorkoutKindService;
import com.SR37.OWP.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("LoyaltyCards")
public class LoyaltyCardController {

    public static final String LOYALTY_POINTS_KEY = "loyaltyCards";

    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @PostConstruct
    public void init(){this.bURL = servletContext.getContextPath()  + "/";}

    @Autowired
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private WorkoutKindService workoutKindService;

    @PostMapping("applyForCard")
    public void apply(HttpSession session, HttpServletResponse response) throws IOException {
        loyaltyCardService.apply(((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername());
        response.sendRedirect(bURL + "Workouts/UserPage");
    }

    @GetMapping("")
    public ModelAndView applicationsPage(HttpSession session,HttpServletResponse response) throws  IOException{
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            ModelAndView resault = new ModelAndView("allWorkoutsPage");
            resault.addObject("kinds",workoutKindService.findAll());
            resault.addObject("workouts",workoutService.findAll());
            resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
            resault.addObject("hasApplied",loyaltyCardService.hasApplied(((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername()));
            return resault;
        }

        ModelAndView resault = new ModelAndView("applicationsPage");
        resault.addObject("users",loyaltyCardService.getAllApplicants());
        return  resault;
    }

    @PostMapping("AcceptApplication")
    public void acceptApplication(@RequestParam String id, HttpServletResponse response) throws IOException {
        loyaltyCardService.acceptApplication(id);
        response.sendRedirect(bURL + "LoyaltyCards");
    }

    @PostMapping("DeclineApplication")
    public void declineApplication(@RequestParam String id, HttpServletResponse response) throws IOException {
        loyaltyCardService.declineApplication(id);
        response.sendRedirect(bURL + "LoyaltyCards");
    }

}
