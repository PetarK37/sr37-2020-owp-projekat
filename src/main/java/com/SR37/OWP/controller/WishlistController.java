package com.SR37.OWP.controller;

import com.SR37.OWP.model.User;
import com.SR37.OWP.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("Wishlist")
public class WishlistController {

    public static final String WISHLIST_KEY = "wishlist";
    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @Autowired
    private WishlistService wishlistService;

    @PostConstruct
    private void init(){this.bURL = servletContext.getContextPath() + "/";}

    @PostMapping("Add")
    public void  addToList(HttpSession session, HttpServletResponse response, @RequestParam int workoutId) throws IOException {
        String username = ((User)(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY))).getUsername();
        wishlistService.addToWishlist(workoutId,username);
        session.setAttribute(WISHLIST_KEY,wishlistService.loadWishedWorkouts(username));
        response.sendRedirect(bURL + "Workouts/UserPage");
    }

    @PostMapping("Remove")
    public void  removeFromList(HttpSession session, HttpServletResponse response, @RequestParam int workoutId) throws IOException {
        String username = ((User)(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY))).getUsername();
        wishlistService.removeFromWishlist(workoutId,username);
        session.setAttribute(WISHLIST_KEY,wishlistService.loadWishedWorkouts(username));
        response.sendRedirect(bURL + "Users/About");
    }

}
