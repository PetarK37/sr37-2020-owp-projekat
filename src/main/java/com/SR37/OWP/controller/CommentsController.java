package com.SR37.OWP.controller;

import com.SR37.OWP.model.Comment;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.CommentService;
import com.SR37.OWP.service.SpecialDateService;
import com.SR37.OWP.service.WorkoutKindService;
import com.SR37.OWP.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping("Comments")
public class CommentsController {
    @Autowired
    private ServletContext servletContext;
    @Autowired
    private SpecialDateService specialDateService;

    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private CommentService commentService;

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private WorkoutKindService workoutKindService;

    private String bURL;

    @PostConstruct
    public void init(){bURL = servletContext.getContextPath() + "/";}

    @GetMapping("GetComments")
    @ResponseBody
    public ArrayList<Comment> getComments(@RequestParam int id) {
        return commentService.findAll(id);
    }

    @GetMapping("AddComment")
    @ResponseBody
    public ModelAndView addCommentPage(@RequestParam int id, HttpServletResponse response, HttpSession session) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN)){
            response.sendRedirect(bURL +"Workouts/AdminPage");
            ModelAndView resault = new ModelAndView("allWorkoutsPage");
            resault.addObject("kinds",workoutKindService.findAll());
            resault.addObject("workouts",workoutService.findAll());
            resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        }

        User user = (User) session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY);
        Workout workout = workoutService.findOne(id);

        ModelAndView resault = new ModelAndView("addCommentPage");
        resault.addObject("Workout",workout);
        resault.addObject("User",user);
        return resault;
    }

    @PostMapping("AddComment")
    @ResponseBody
    public Map<String ,Object> addCommentPage(@RequestParam boolean anonymous,@RequestParam int workoutId, @RequestParam(required = false) String username,@RequestParam String content,@RequestParam float rating){
        Map<String,Object> response = new LinkedHashMap<>();
        Comment newComment = new Comment(content, rating,username,workoutService.findOne(workoutId));
        if (commentService.save(newComment,anonymous)){
            response.put("status","ok");
        }else {
            response.put("status","error");
        }
        return response;
    }

    @GetMapping("GetApplications")
    @ResponseBody
    public ArrayList<Comment> getApplications(){
        return commentService.findAllCommentApplications();
    }

    @GetMapping("Applications")
    @ResponseBody
    public ModelAndView getApplicationsPage(HttpSession session,HttpServletResponse response) throws IOException {

        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            ModelAndView resault = new ModelAndView("allWorkoutsPage");
            resault.addObject("kinds",workoutKindService.findAll());
            resault.addObject("workouts",workoutService.findAll());
            resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        }
        ModelAndView resault = new ModelAndView("commentApplicationsPage");
        resault.addObject("comments",commentService.findAllCommentApplications());
        return resault;
    }


    @PostMapping("Accept")
    @ResponseBody
    public void accept(@RequestParam int id,HttpServletResponse response) throws IOException {
        Comment updateComment =  commentService.findOne(id);
        if (commentService.acceptComment(updateComment)){
            response.sendRedirect(bURL + "Comments/Applications");
        }
    }

    @PostMapping("Decline")
    @ResponseBody
    public void decline(@RequestParam int id,HttpServletResponse response) throws IOException {
        Comment updateComment =  commentService.findOne(id);
        if (commentService.declineComment(updateComment)){
            response.sendRedirect(bURL + "Comments/Applications");
        }
    }

}
