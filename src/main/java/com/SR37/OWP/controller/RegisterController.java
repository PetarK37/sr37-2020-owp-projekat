package com.SR37.OWP.controller;

import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.LoginAndRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

@Controller
public class RegisterController {

    @Autowired
    private LoginAndRegisterService loginAndRegisterService;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private LocaleResolver localeResolver;

    private String bURL;

    @PostConstruct
    public void init(){bURL = servletContext.getContextPath() + "/";}

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @GetMapping("signUp")
    public String signUpWindow(){
     return "signUpPage";
    }

    @PostMapping(value = "signUp")
    public ModelAndView signUp(HttpServletRequest request, @RequestParam(required = false) String name, @RequestParam(required = false) String surname
    , @RequestParam(required = false) String email, @RequestParam(required = false) String username,
                               @RequestParam(required = false) String password, @RequestParam(required = false) String phoneNum,
                               @RequestParam(required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date , @RequestParam(required = false) String address, HttpServletResponse response) throws IOException {

        User newUser = new User(username,password,email,name,surname, date ,address,phoneNum, EUserRole.USER);

        if (loginAndRegisterService.register(newUser)){
             response.sendRedirect(bURL);
        }

        ModelAndView resault = new ModelAndView( "signUpPage");
        resault.addObject("error", true);
        if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
            resault.addObject("errorTxt","there is already user with that username/email!");

        }else {
            resault.addObject("errorTxt","vec postoji korisnik sa tim kor.imenom/e-mailom");

        }
        return resault;
    }
}
