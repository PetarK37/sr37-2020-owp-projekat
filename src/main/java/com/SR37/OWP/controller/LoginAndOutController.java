package com.SR37.OWP.controller;

import com.SR37.OWP.configurations.LocaleConfig;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

@Controller
public class LoginAndOutController {

    public static final String  LOGGED_IN_USER_KEY = "loggedInUser";
    @Autowired
    private ServletContext servletContext;

    @Autowired
    private LoginAndRegisterService loginAndRegisterService;

    @Autowired
    private UsersService usersService;

    private String bURL;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    LoyaltyCardService loyaltyCardService;

    @Autowired
    private WishlistService wishlistService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostConstruct
    public void init(){bURL = servletContext.getContextPath() + "/";}

    @GetMapping("/logIn")
    public ModelAndView logIn(boolean hasError,String error,@RequestParam(defaultValue = "sr") String lang,HttpServletRequest request,HttpServletResponse response){
        return index(hasError,error,lang,request,response);
    }

    @GetMapping("")
    public ModelAndView index(boolean hasError,String error,@RequestParam(defaultValue = "sr") String lang,
                              HttpServletRequest request,HttpServletResponse response){


        localeResolver.setLocale(request,response, Locale.forLanguageTag(lang));
        ModelAndView resault = new ModelAndView( "login");
        resault.addObject("error",hasError);
        resault.addObject("errorTxt",error);
        return resault;
    }

    @PostMapping("/logIn")
    public ModelAndView logIn(@RequestParam(required = false) String username , @RequestParam(required = false) String password
            , HttpSession session, HttpServletResponse response,HttpServletRequest request,@RequestParam(defaultValue = "sr") String lang) throws IOException {

       if (session.getAttribute(LOGGED_IN_USER_KEY ) != null){
           ModelAndView resault = new ModelAndView("alreadyLogedInPage");
           if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
               resault.addObject("userMsg", ( "You already are logged in as " + ((User)session.getAttribute(LOGGED_IN_USER_KEY )).getName() + "," + ((User) session.getAttribute(LOGGED_IN_USER_KEY)).getSurname()));
           }else {
               resault.addObject("userMsg", ( "Vec ste ulogovani kao " + ((User)session.getAttribute(LOGGED_IN_USER_KEY )).getName() + "," + ((User) session.getAttribute(LOGGED_IN_USER_KEY)).getSurname()));
           }
           return resault;
       }

       if (loginAndRegisterService.logIn(username,password)){
           Integer loyaltyPoints = loyaltyCardService.getMyLoyaltyPoints(username);
           if (loyaltyPoints != null){
               session.setAttribute(LoyaltyCardController.LOYALTY_POINTS_KEY,loyaltyPoints);
           }

           User loggedInUser = usersService.findOne(username);
           ShoppingCart shoppingCart = shoppingCartService.findOne(username);
           session.setAttribute(LOGGED_IN_USER_KEY,loggedInUser);
           session.setAttribute(WishlistController.WISHLIST_KEY,wishlistService.loadWishedWorkouts(username));
           session.setAttribute(ShoppingCartController.SHOPPING_CART_KEY,shoppingCart);

           if (loggedInUser.getUserRole().equals(EUserRole.USER)){
               response.sendRedirect("Workouts/UserPage");
           }else{
               response.sendRedirect("Workouts/AdminPage");
           }
       }
       if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)){
           return index(true,"The username or password are not correct!",localeResolver.resolveLocale(request).toLanguageTag(),request,response);
       }
        return index(true,"Korisnicko ime ili lozinka nisu dobri!",localeResolver.resolveLocale(request).toLanguageTag(),request,response);
    }

    @PostMapping("logOut")
    public void logOut(HttpSession session,HttpServletResponse response,HttpServletRequest request) throws IOException {
        session.setAttribute(LOGGED_IN_USER_KEY,null);
        session.setAttribute(LoyaltyCardController.LOYALTY_POINTS_KEY,null);
        session.setAttribute(WishlistController.WISHLIST_KEY,new ArrayList<Workout>());
        response.sendRedirect(bURL + "?lang=" + localeResolver.resolveLocale(request));

    }

}
