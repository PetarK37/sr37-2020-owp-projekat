package com.SR37.OWP.controller;

import com.SR37.OWP.model.Reservation;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.ShoppingCart;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping("ShoppingCart")
public class ShoppingCartController {

    public static final String  SHOPPING_CART_KEY = "shoppingCart";

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private WorkoutKindService workoutKindService;
    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private SpecialDateService specialDateService;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    private ScheduleService scheduleService;


    private String bURL;

    @PostConstruct
    public void init(){bURL = servletContext.getContextPath() + "/";}

    @GetMapping
    public ModelAndView shoppingCart(HttpSession session,HttpServletResponse response,@RequestParam(required = false) Integer loyaltyPoints) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN)){
            response.sendRedirect(bURL +"Workouts/AdminPAge");
            ModelAndView resault = new ModelAndView("allWorkoutsPage");
            resault.addObject("kinds",workoutKindService.findAll());
            resault.addObject("workouts",workoutService.findAll());
            resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
    }
        ModelAndView resault = new ModelAndView("shoppingCartPage");
        if (loyaltyPoints != null){
            session.setAttribute(SHOPPING_CART_KEY,shoppingCartService.findOne(((User) session.getAttribute("loggedInUser")).getUsername()));
            ((ShoppingCart)session.getAttribute(SHOPPING_CART_KEY)).recalculatePrice(loyaltyPoints);
            ((ShoppingCart)session.getAttribute(SHOPPING_CART_KEY)).setLoyaltyPoints(loyaltyPoints);
        }
        resault.addObject("shoppingCart",(((ShoppingCart)session.getAttribute(SHOPPING_CART_KEY))));
        resault.addObject("isSpecialDate", specialDateService.isSpecialDate(((ShoppingCart)session.getAttribute(SHOPPING_CART_KEY))));

        return resault;
    }

    @PostMapping("AddToCart")
    @ResponseBody
    public Map<String,Object> addToCart(@RequestParam int scheduleId, HttpSession session) throws IOException {
        String username = ((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername();
        Map<String,Object> response = new LinkedHashMap<>();
        if (shoppingCartService.addToChart(scheduleService.findOne(scheduleId),username)){
            session.setAttribute(SHOPPING_CART_KEY,shoppingCartService.findOne(username));

            response.put("status","ok");
            return response;
        }
        response.put("status","error");
        return response;
    }

    @PostMapping("Remove")
    public void removeFromCart(@RequestParam int scheduleId, HttpSession session,HttpServletResponse response)throws IOException {
        String username = ((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername();
        shoppingCartService.removeFromChart(scheduleId,username);
        session.setAttribute(SHOPPING_CART_KEY,shoppingCartService.findOne(username));
        response.sendRedirect(bURL+ "ShoppingCart");
    }

    @PostMapping("Reserve")
    public void reserve(HttpSession session,HttpServletResponse response)throws IOException {
        User user = ((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY));
        ShoppingCart sc = (ShoppingCart)(session.getAttribute(SHOPPING_CART_KEY));
        for (Schedule s : sc.getSchedules()){
            reservationService.save(new Reservation(s,user,s.getWorkout().getPrice()));
        }
        if (!specialDateService.isSpecialDate(sc) && session.getAttribute(LoyaltyCardController.LOYALTY_POINTS_KEY) != null){
            loyaltyCardService.removePoints(user.getUsername(),(int)(sc.getLoyaltyPoints()));
            loyaltyCardService.addPoints(user.getUsername(),Math.round(sc.getPrice())/500);
        }
        shoppingCartService.clear(user.getUsername());
        session.setAttribute(SHOPPING_CART_KEY,shoppingCartService.findOne(user.getUsername()));

        response.sendRedirect(bURL+ "Workouts/UserPage");
    }
}
