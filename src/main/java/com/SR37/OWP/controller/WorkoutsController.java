package com.SR37.OWP.controller;

import com.SR37.OWP.model.ImageDTO;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.Workout;
import com.SR37.OWP.model.WorkoutSearchDTO;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.model.enums.EWorkoutLevel;
import com.SR37.OWP.model.enums.EWorkoutType;
import com.SR37.OWP.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@Controller
@RequestMapping("/Workouts")
public class WorkoutsController {
    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @PostConstruct
    public void init(){this.bURL = servletContext.getContextPath()  + "/";}

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private WorkoutKindService workoutKindService;

   @Autowired
    private ImageService imageService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private ReservationService reservationService;

    @GetMapping()
    public ModelAndView index(HttpSession session,HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) != null){

           if ((((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN))){
                response.sendRedirect(bURL + "Workouts/AdminPage");
            }
            response.sendRedirect(bURL +"Workouts/UserPage");
        }
        ModelAndView resault = new ModelAndView("allWorkoutsPage");
        resault.addObject("kinds",workoutKindService.findAll());
        resault.addObject("workouts",workoutService.findAll());
        resault.addObject("role","GUEST");
        return resault;
    }

    @GetMapping("/UserPage")
    public ModelAndView allWorkoutsPage(HttpSession session,HttpServletResponse response) throws IOException {
       if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
           response.sendRedirect(bURL);
           return new ModelAndView("login");
       }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN)){
            response.sendRedirect(bURL + "Workouts/AdminPage");
        }
        ModelAndView resault = new ModelAndView("allWorkoutsPage");
        resault.addObject("kinds",workoutKindService.findAll());
        resault.addObject("workouts",workoutService.findAll());
        resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        resault.addObject("hasApplied",loyaltyCardService.hasApplied(((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername()));
        return resault;
    }

    @GetMapping("/AdminPage")
    public ModelAndView allWorkoutsPageAdmin(HttpSession session,HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
        }
        ModelAndView resault = new ModelAndView("allWorkoutsPage");
        resault.addObject("kinds",workoutKindService.findAll());
        resault.addObject("workouts",workoutService.findAll());
        resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        return resault;
    }



    @GetMapping("Search")
    public ModelAndView search(@RequestParam String name, @RequestParam String type, @RequestParam String level,
                               @RequestParam String instructors, @RequestParam String kind
                                ,@RequestParam float priceFrom,@RequestParam float priceTo,
                               @RequestParam String order, @RequestParam String orderCriterium,HttpSession session){

        ModelAndView resault = new ModelAndView("allWorkoutsPage");
        resault.addObject("role",(User)session.getAttribute("loggedInUser") == null ? "GUEST" : ((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        resault.addObject("kinds",workoutKindService.findAll());
        resault.addObject("workouts",workoutService.find(new WorkoutSearchDTO(name == "" ?"%%":name,type== "" ?"%%":type,level== "" ?"%%":level,instructors== "" ?"%%":instructors
        ,kind== "" ?"%%":kind,priceFrom,priceTo,orderCriterium == "" ?"'%%'":orderCriterium,order== "" ? "asc" : order)));
        return resault;
    }

    @GetMapping("Details")
    public ModelAndView details(@RequestParam int id, HttpSession session,HttpServletResponse response) throws IOException {

        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) != null){
            ModelAndView resault = new ModelAndView( "detailsPage");
            resault.addObject("scheduales", scheduleService.findAllCurrent(id));
            resault.addObject("workout",workoutService.findOne(id));
            resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
            resault.addObject("hasApplied",loyaltyCardService.hasApplied(((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername()));
            resault.addObject("canComment",reservationService.findByWorkout(((User)session.getAttribute("loggedInUser")).getUsername(),id) != null
                    && !commentService.allreadyCommented(((User)session.getAttribute("loggedInUser")).getUsername(),id));
            return resault;
        }

        ModelAndView resault = new ModelAndView( "detailsPage");
        resault.addObject("scheduales", scheduleService.findAllCurrent(id));
        resault.addObject("workout",workoutService.findOne(id));
        resault.addObject("role","GUEST");
        return resault;

    }

    @GetMapping("AddWorkout")
    public ModelAndView addWorkout(HttpSession session,HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        else if(!((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN))
        {
            response.sendRedirect(bURL + "Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");

        }
        ModelAndView resault = new ModelAndView( "addNewWorkoutPage");
        resault.addObject("kinds",workoutKindService.findAll());
        resault.addObject("workouts",workoutService.findAll());
        return resault;
    }

    @PostMapping("AddWorkout")
    public void addWorkout(@RequestParam String name, @RequestParam String type, @RequestParam String level,
                           @RequestParam String instructors, @RequestParam ArrayList<Integer> kindId
            , @RequestParam float price, @RequestParam String description, @RequestParam(required = false) MultipartFile img
    , HttpServletResponse response) {

        try {
            ImageDTO image = new ImageDTO(img);
            if(!image.getFilename().equals("")){
                imageService.Save(image);
            }
            Workout newWorkout = new Workout(name, instructors, description, image.getFilename().equals("") ? "/img/default-placeholder.png" : image.getPath(),price, EWorkoutType.valueOf(type)
            , EWorkoutLevel.valueOf(level), workoutKindService.findAll(kindId));
           long id = workoutService.save(newWorkout);
           newWorkout.setId(id);
            workoutKindService.saveAllKindsPerWorkout(newWorkout);
            response.sendRedirect( bURL + "Workouts/AdminPage");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/EditWorkout")
    public ModelAndView editPageGet(@RequestParam int id,HttpSession session,HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        else if(!((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN))
        {
            response.sendRedirect(bURL + "Workouts/UserPage");
            return new ModelAndView("allWorkoutsPage");

        }

        ModelAndView resault = new ModelAndView( "editWorkoutPage");
        resault.addObject("workout",workoutService.findOne(id));
        resault.addObject("kinds",workoutKindService.findAll());
        return resault;
    }

    @PostMapping("/EditWorkout")
    public void editWorkoutPost(@RequestParam int id,@RequestParam String name, @RequestParam String type, @RequestParam String level,
                                @RequestParam String instructors, @RequestParam float price, @RequestParam ArrayList<Integer> kindId,
                                @RequestParam String description, @RequestParam(required = false) MultipartFile img
                                , HttpServletResponse response){

        try {
            ImageDTO image = new ImageDTO(img);
            if(!image.getFilename().equals("")){
                imageService.Save(image);
            }
            Workout workout = workoutService.findOne(id);
            workout.update(name,instructors,description, image.getFilename().equals("") ? "" : image.getPath(), price,EWorkoutType.valueOf(type),EWorkoutLevel.valueOf(level),workoutKindService.findAll(kindId));
            workoutService.update(workout);
            response.sendRedirect( bURL + "Workouts/AdminPage");

    } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @GetMapping("KindDetails")
    public ModelAndView kindPage(@RequestParam int id, HttpSession session){
        ModelAndView resault = new ModelAndView( "workoutKindPage");
        resault.addObject("kind",workoutKindService.findOne(id));
        resault.addObject("role",(User)session.getAttribute("loggedInUser") == null ? "GUEST" : ((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        return resault;
    }
}
