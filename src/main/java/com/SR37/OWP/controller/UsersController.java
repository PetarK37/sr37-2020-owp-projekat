package com.SR37.OWP.controller;

import com.SR37.OWP.model.Reservation;
import com.SR37.OWP.model.Schedule;
import com.SR37.OWP.model.User;
import com.SR37.OWP.model.UserSearchDTO;
import com.SR37.OWP.model.enums.EUserRole;
import com.SR37.OWP.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Locale;

@Controller
@RequestMapping("Users")
public class UsersController {

    @Autowired
    private ServletContext servletContext;
    private String bURL;

    @Autowired
    private UsersService usersService;

    @Autowired
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    private WorkoutKindService workoutKindService;
    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private WishlistService wishlistService;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private ReservationService reservationService;

    @PostConstruct
    private void init(){this.bURL = servletContext.getContextPath() + "/";}

    @GetMapping("AllUsers")
    public ModelAndView allUsers(HttpSession session, HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER)){
            response.sendRedirect(bURL +"Workouts/UserPage");
            ModelAndView resault = new ModelAndView("allWorkoutsPage");
            resault.addObject("kinds",workoutKindService.findAll());
            resault.addObject("workouts",workoutService.findAll());
            resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
            resault.addObject("hasApplied",loyaltyCardService.hasApplied(((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername()));
        }

        ModelAndView resault = new ModelAndView("allUsersPage");
        resault.addObject("users",usersService.findAll());
        return resault;
    }

    @GetMapping("/Search")
    public ModelAndView search(@RequestParam String username,@RequestParam(required = false) EUserRole userRole,
                               @RequestParam String order, @RequestParam String orderCriterium,
                               HttpSession session){

        ModelAndView resault = new ModelAndView("allUsersPage");
        resault.addObject("users", usersService.findAll(new UserSearchDTO(username == "" ?"%%": username
                ,userRole,orderCriterium  == "" ? "%%": orderCriterium,order == "" ? "%%" : order)));
        return resault;
    }

    @PostMapping("Block")
    public void blockUser(@RequestParam String username, @RequestParam boolean status,HttpServletResponse response) throws IOException {
        usersService.applyBlocStatus(status,username);
        response.sendRedirect(bURL + "Users/AllUsers");
    }

    @GetMapping("About")
    public ModelAndView aboutUser(HttpSession session,HttpServletResponse response,@RequestParam(required = false) String username,boolean error,String errorTxt,HttpServletRequest request) throws IOException {
        ModelAndView resault = new ModelAndView("aboutUserPage");
        resault.addObject("error",error);
        resault.addObject("errorTxt", errorTxt);
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            response.sendRedirect(bURL);
            return new ModelAndView("login");
        }
        if ((((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN))){
            resault.addObject("user", usersService.findOne(username));
            resault.addObject("reservations",reservationService.findAll(username));

        }else {
            resault.addObject("user", (User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY));
            resault.addObject("reservations",reservationService.findAll(((User) session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername()));

        }
        if (((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN) && username.equals(((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername())){
            resault.addObject("error",true);
            if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)) {
                resault.addObject("errorTxt", "You can not edit yourself!");
            }
                resault.addObject("errorTxt", "Ne mozete menjati sami sebe!");
        }
        resault.addObject("role",((User)session.getAttribute("loggedInUser")).getUserRole().toString());
        resault.addObject("wishlist",wishlistService.loadWishedWorkouts(((User) session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername()));
        return resault;
    }

    @PostMapping("About/Edit")
    public ModelAndView editUser(HttpSession session, HttpServletResponse response, HttpServletRequest request,
                                 @RequestParam(required = false) String name, @RequestParam(required = false) String surname
            , @RequestParam(required = false) String email, @RequestParam(required = false) String username,
                                 @RequestParam(required = false) String phoneNum,
                                 @RequestParam(required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date
            , @RequestParam(required = false) String address, @RequestParam(required = false) EUserRole role
            ,@RequestParam String begginigUsername,@RequestParam String begginigEmail) throws IOException {

        User userForEdit = usersService.findOne(begginigUsername);

        userForEdit.Update(username,email,name,surname,date,address,phoneNum,role);

        if (usersService.update(userForEdit,begginigUsername,begginigEmail)){
            if ((((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.USER))){
                session.setAttribute(LoginAndOutController.LOGGED_IN_USER_KEY,usersService.findOne(username));
            }
            return aboutUser(session,response,username,false,"",request);
        }
        if(localeResolver.resolveLocale(request).equals(Locale.ENGLISH)) {
            return aboutUser(session,response,begginigUsername,true, "there is already user with that username/email!",request);
        }
            return aboutUser(session,response,begginigUsername,true,"vec postoji korisnik sa tim kor.imenom/e-mailom",request);


    }

    @GetMapping("About/Edit/Password")
    public ModelAndView editPassword(HttpSession session,HttpServletResponse response) throws IOException {
        if(session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY) == null){
            return new ModelAndView("login");
        }
        if ((((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUserRole().equals(EUserRole.ADMIN))){
            ModelAndView resault = new ModelAndView("allUsersPage");
            resault.addObject("users",usersService.findAll());
            response.sendRedirect(bURL + "Users/AllUsers");
            return  resault;
        }
        return new ModelAndView("changePasswordPage");
    }

    @PostMapping("About/Edit/Password")
    public void changePassword(@RequestParam String password,HttpServletResponse response,HttpSession session) throws IOException {
        if (usersService.updatePassword(password,((User)session.getAttribute(LoginAndOutController.LOGGED_IN_USER_KEY)).getUsername())){
            response.sendRedirect(bURL + "Users/About");
        }
    }
}
