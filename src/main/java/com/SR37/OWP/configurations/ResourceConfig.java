package com.SR37.OWP.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class ResourceConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**","/img/**","/css/**", "/js/**").addResourceLocations("classpath:/META-INF/resources/webjars/","classpath:/static/css/", "classpath:/static/js/","classpath:src/main/resources/img","file:F:\\Projekat sr37\\Projekat_SR_37\\src\\main\\resources\\img\\");
    }
}
