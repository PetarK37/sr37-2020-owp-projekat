package com.SR37.OWP.listeners;

import com.SR37.OWP.controller.LoyaltyCardController;
import com.SR37.OWP.controller.WishlistController;
import com.SR37.OWP.model.Workout;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class InitHttpSessionListener implements HttpSessionListener {
    public void sessionCreated(HttpSessionEvent arg0) {
        HttpSession session = arg0.getSession();
        session.setAttribute(LoyaltyCardController.LOYALTY_POINTS_KEY,null);
        session.setAttribute(WishlistController.WISHLIST_KEY,new ArrayList<Workout>());
    }

    /** kod koji se izvrsava po brisanju sesije */
    public void sessionDestroyed(HttpSessionEvent arg0) {
        System.out.println("deleting session");
    }
}
