DROP SCHEMA IF EXISTS flexGymDatabase;
CREATE SCHEMA flexGymDatabase DEFAULT CHARACTER SET utf8;
USE flexGymDatabase;

create table users(
	username varchar(255) primary key,
    _password varchar(255) not null,
     email varchar(100) not null unique,
    _name varchar(255) not null,
    surname varchar(255) not null,
    birthday date not null,
    addres varchar(555) not null,
    phone_num varchar(30) not null unique,
    dateTimeOfRegistration dateTime not null,
    _role enum('ADMIN','USER') default 'USER',
    isBlocked boolean default false
);

create table cardApplications(
	userId varchar(255) not null,
    foreign key(userId) references users(username) on delete cascade on update cascade,
	primary key (userId)
);

create table loyaltyCards (
	userId varchar(255) ,
    points int default 10,
    foreign key(userId) references users(username) on delete cascade,
    primary key (userId)
);

create table workouts (
	workoutId bigint auto_increment primary key, 
	_name varchar (555) not null,
    _description TEXT not null,
    instructors varchar(2000) not null,
    img_url varchar (1000),
    price float,
    avgRating float default 0,
    _type Enum ('GRUPNI','POJEDINAČNI') default 'POJEDINAČNI',
    _level Enum ('AMATERSKI','SREDNJI','NAPREDNI') default 'AMATERSKI' 
);

create table wishlist (
	workoutId bigint not null,
    userId varchar(255) not null,
    foreign key(workoutId) references workouts(workoutId) on delete cascade,
    foreign key(userId) references users(username) on delete cascade on update cascade,
    primary key (userId,workoutId)
);

create table workoutKinds (
 kindId bigint auto_increment primary key,
 kindName varchar(255) not null,
 kindDescription TEXT
);

create table kindPerWorkout(
	kindId bigint not null,
    workoutId bigint not null,
	foreign key(workoutId) references workouts(workoutId) on delete cascade,
    foreign key(kindId) references workoutKinds(kindId) on delete cascade,
    primary key(kindId,workoutId)
);

create table rooms(
	roomId varchar(15) primary key,
    capacity int,
    isDeleted bool default false
);

create table scheduales (
	schedualeId bigint auto_increment primary key,
    workoutId bigint not null,
    roomId varchar(15) not null,
    _dateTime  dateTime not null,
    endDateTime dateTime not null,
    isFree boolean default true,
    foreign key(workoutId) references workouts(workoutId) on delete cascade,
    foreign key(roomId) references rooms(roomId) on delete cascade
);


create table reservations(
	userId varchar(255),
    schedualeId bigint,
    _date  DateTime not null,
    currentPrice float,
    foreign key(userId) references users(username) on delete cascade on update cascade,
    foreign key (schedualeId) references scheduales(schedualeId) on delete cascade on update cascade,
	primary key(userId,schedualeId)
);

create table shoppingCart(
	userId varchar(255),
    schedualeId bigint,
    foreign key(userId) references users(username) on delete cascade on update cascade,
    foreign key (schedualeId) references scheduales(schedualeId) on delete cascade on update cascade,
	primary key(userId,schedualeId)
);

create table comments (
	commentId bigint auto_increment primary key,
	workoutId bigint not null,
    content varchar(3000) not null,
    rating int not null,
    _date date not null,
	userId varchar(255),
    _status enum('NA_ČEKANJU','ODOBREN','NIJE_ODOBREN') default 'NA_ČEKANJU', 
	foreign key(workoutId) references workouts(workoutId) on delete cascade,
	foreign key(userId) references users(username)on delete cascade on update cascade
);

create table specialDates(
	startDate Date,
    endDate Date,
    workoutId bigint,
    discount int,        
    foreign key(workoutId) references workouts(workoutId) on delete cascade,
    primary key (startDate,endDate,workoutId)
);

insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration,_role) values('pera123','1234','peric@gmail.com','pera','peric','1989-05-12','Strazilovska 21, Novi Sad','+381624406228',now(),'ADMIN');
insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration,_role) values('mika123','1234','mika@gmail.com','mika','peric','1997-12-10','Maksima Gorkog 2, Novi Sad','+381624807528',now(),'ADMIN');
insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration,isBlocked) values('ivan123','1234','bojicic@gmail.com','ivan','bojicic','2001-02-12','Kisacka 13, Novi Sad','+381615706228',now(),true);
insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration) values('jovana123','1234','jockovic@gmail.com','jovana','jockovic','1975-07-23','Bulevar Oslobodjenja 114, Novi Sad','+381635756288',now());
insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration) values('milos123','1234','tomic@gmail.com','milos','tomis','1992-07-06','Bulevar Oslobodjenja 34, Novi Sad','+381643372883',now());
insert into users(username,_password,email,_name,surname,birthday,addres,phone_num,dateTimeOfRegistration) values('petar123','1234','petrovic@gmail.com','petar','petrovic','2000-11-02','Cirpanova 6, Novi Sad','+3816038347335',now());


insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values ('Jogi pilates','Trening koji je možda najcelovitiji sinonim fitnesa generalno. Daje rezultate u povećanju elastičnosti, snage i energije, svesnosti sopstvenog tela, poboljšanje mentalne koncentracije. Utiče na poboljšanje ravnoteže i koriguje nepravilno držanje kičme. Preporučuje se početnicima i naprednim rekreativcima.','/img/best-beginner-weight-training-guide-with-easy-to-follow-workout-header-b-960x540.jpg','milan maric,jovana tasovac',1300,'POJEDINAČNI','SREDNJI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Vezbe snage sa milanom','Vežbanje ili fizička aktivnost veoma je važna za snažan podsticaj i za pojavu adaptacije. Glavni cilj treninga podrazumeva kreiranje specifičnih adaptacionih procesa kako bi se poboljšali sportski rezultati. Sve ovo znači da trening je potrebno pažljivo isplanirati. Sa praktične tačke gledišta za sportski trening su najvažniji sledeći elementi:,stepen stimulusa (nadopterećenje),prilagođavanje,specifičnost,individualizacija','/img/default-placeholder.png','milan maric',600,'GRUPNI','SREDNJI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Health joga','Kao najrasprostranjeniji vid joge u svetu Hatha se odnosi na praktikovanje joga položaja (asana), vežbi disanja (pranayama) i tehnika opuštanja, tako da se različiti stilovi joge (Ashtanga, Vinyasa,Bikram, power joga…) mogu svrstati pod Hatha jogu.Reč „Hatha“ na Sanskritu znači snažan, jak. „Ha“ predstavlja Sunce i mušku ekstrovertnu energiju, dok „Tha“ simbolizuje Mesec i pasivnu, introvertnu žensku energiju.','/img/default-placeholder.png','milan maric,ivan markovic,nikola prokic',1900,'POJEDINAČNI','SREDNJI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Krosfit sa nikolom','Ako ste odlučili da svoje slobodno vreme posvetite baš krosfit treninzima, znajte da nećete pogrešiti. Svaka odluka je dobra ukoliko se tiče vašeg zdravlja, pogotovo ako ste vlasnik FitPass kartice. Ova sportska disciplina je izuzetno popularna, bez obzira da li ste početnik ili ne. Javite nam utiske ukoliko vam je ovaj tekst rešio određene dileme i nedoumice.','/img/28468296_1689763461085366_1930937440349460629_n.jpg','nikola prokic',2000,'POJEDINAČNI','SREDNJI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Napredna joga','Napredni nivo vežbanja je izazov više za one koji su neko vreme praktikovali jogu. Vežbe su namenjene iskusnim vežbačima joge, ali zbog brojnih varijacija izvođenja mogu biti prilagođene sopstvenim (trenutnim) mogućnostima.','img/vrednost_i_sustina_joge_vasi_stavovi_slabosti_i_namere_na_joga_prostirci_1.jpg','ivan markovic',1450,'GRUPNI','NAPREDNI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Joga za pocetnike','Joga je jedan od najboljih tipova fizičke aktivnosti sa izuzetno bogatom tradicijom: veruje se da postoji preko 5000 godina. Joga jača mišiće, deluje smirujuće i poboljšava rad imunog sistema. Što je još bolje, jogom mogu da se bave svi - bez obzira na uzrast i nivo fizičke spreme. Za jogu su vam potrebni samo podloga za vežbanje, udobna odeća, dovoljno prostora, kao i motivacija da pravilno savladate određene poze. Da li smo vas već zainteresovali? Predstavljamo vam savršene vežbe za početnike.','/img/default-placeholder.png','dragan simic,ivan maric',1600,'POJEDINAČNI','AMATERSKI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Power Pilates','Napredniji oblik pilatesa, trening koji se radi kroz odgovarajuću koreografij i formu.. Gradi snagu bez dodatnog napora, stvarajući vitko telo sa zategnutim butinama i ravnim stomakom. Poboljšava fleksibilnost, snagu i efikasnost kretanja. To je sklop vezbi koje podrazumevaju rad na koordinaciji, gipkosti i snazi. Pažnja se fokusira na stav tela, ravnotežu i koncentraciju prilikom vežbanja. Dovedite svoje telo u optimalnu kondiciju, zategnite mišiće I povećajte gipkost.','/img/pilates-devojke-Getty.jpg','dragan simic',2200,'POJEDINAČNI','AMATERSKI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Evening Energizer','Napredan metod vezbanja koji spaja savremene tehnike vezbanja s opterecenjima sa drevnim tehikama istezanja i pravilnog disanja. Ovo nacinom vezbanja postize se balans spoljasnjen,vidljivog dela organizma i pravilnog funkcionisanja unutrasnjih vitalnih organa','/img/pilates-devojke-Getty.jpg','dragan simic,jovana tasovac,ivan maric',800,'POJEDINAČNI','AMATERSKI');
insert into workouts(_name,_description,img_url,instructors,price,_type,_level) values('Cardio Pilates','Program vežbi kojima možete preoblikovati vaše telo, izgubiti masti, izdefinisati mišiće, poboljšati vašu izdržljivost. Kombinujući aerobne vežbe sa vežbama pilatesa lakše postižemo definiciju cele muskulture jer aerobnim vežbama dovodimo organizam u stanje gde kao izvor energije troši i smanjuje masti, dok pilates vežbama postižemo snagu i definiciju mišića. Cardio Pilates je jedan od najboljih načina da izgubite višak kilograma i da izgradite mišiće. U isto vreme topimo masti i razvijamo mišićnu muskulturu. U pilatesu je akcenat na preciznosti, kvalitetu pokreta i razvoju svesti o našem telu','img/default-placeholder.png','ivan maric',1200,'GRUPNI','AMATERSKI');

insert into workoutkinds(kindName,kindDescription) values ('Joga' , 'Joga je nauka o telu, umu i duhu. Fizički deo Joge sastoji se iz Asana - vežbi i položaja koji razvijaju telo i Pranajama - vežbi disanja. Mentalni razvoj u Jogi postiže se kroz vežbe disanja, vežbe koncentracije i meditaciju.');
insert into workoutkinds(kindName,kindDescription) values ('Krosfit' , 'Crossfit (krosfit) je veoma intenzivni program vežbanja koji podrazumeva veoma dinamične vežbe kao što su pliometrijski skokovi i olimpijsko dizanje ne tradicionalnih utega kao što su vreće napunjene peskom ili flaše napunjene vodom. Program je osmišljen tako da svi koji se u njega upuste treba da urade određeni broj ponavljanja vežbe u zadanom vremenskom roku. Oni malo napredniji se u stvari takmiče jedni protiv drugih da vide koliko brzo mogu odraditi svoje dnevne vežbe, a rezultati se stavljalju i na sajt posvećen ovoj vrsti vežbe.');
insert into workoutkinds(kindName,kindDescription) values ('Kardio' , 'Kardio trening je najefikasniji način za skidanje masnih naslaga, postizanje i održavanja kondicije. Kardio-vežbe su neophodne za svaki efikasan program treninga jer podstiču sagorevanje masnoća ali su ujedno dobre i za celokupno zdravlje organizma. Takođe, ukoliko dugo niste vežbali, i nemate snage za druge treninge, kardio trening uvek možete prilagoditi svojim mogućnostima.');
insert into workoutkinds(kindName,kindDescription) values ('Vezbe snage' , 'Trening snage je jedan od najefikasnijih sagorevača kalorija. Veća snaga ne znači automatsko povećanje mišićne mase ali su snaga i mišići usko povezani sa određenim režimom ishrane, pa je tako dobro imati veću snagu ako želite napredak u mišićnoj masi.
Ako govorimo o vežbama snage i benfitima koje povećanje snage donosi organizmu, moramo da se osvrnemo i ne neke osnovne kriterijume bezbednosti i sigurnosti svakog vežbača. Povrede nastaju kao posledica nepravilnog izvođenja vežbi, nestručne asistencije ili slučajnih nezgoda. Zaštita od povreda je veoma važan faktor, i zato je najvažnije smanjiti ili potpuno izbeći rizik od povreda. Povrede se uglavnom odnose na mišićna i vezivna tkiva i mogu se sprečiti odgovarajućim merama predostrožnosti.');
insert into workoutkinds(kindName,kindDescription) values ('Pilates' , 'Pilates je naziv za sistem fizičkog treninga kog je početkom XX. veka u Nemačkoj osmislio Jozef Pilates.Pilatesova ideja bila je da stvori sistem vežbanja u kom mišiće kontroliše um zbog čega ova tehnika predstavlja vrlo privlačnu kombinaciju istočnjačkog i zapadnjačkoig pristupa vežbanju. Program se fokusira na mišiće koji pomažu održavanju ispravne posture i predstavljaju oslonac za kičmu. Još konkretnije, pilates podučava svesnosti o dahu, skladnom stavu kičme i jačanju mišića torza.');

insert into rooms(roomId,capacity) values('11b' , 12);
insert into rooms(roomId,capacity) values('1' , 8);
insert into rooms(roomId,capacity) values('2' , 5);
insert into rooms(roomId,capacity) values('3c' , 2);
insert into rooms(roomId,capacity) values('4' , 3);

insert into wishlist(workoutId,userId) values (2,'ivan123');
insert into wishlist(workoutId,userId) values (5,'ivan123');
insert into wishlist(workoutId,userId) values (9,'ivan123');
insert into wishlist(workoutId,userId) values (2,'jovana123');
insert into wishlist(workoutId,userId) values (1,'jovana123');

insert into cardApplications(userId) values ('ivan123');

insert into loyaltyCards(userId) values ('jovana123');

insert into kindPerWorkout(kindId,workoutId) values(1,1);
insert into kindPerWorkout(kindId,workoutId) values(5,1);
insert into kindPerWorkout(kindId,workoutId) values(3,2);
insert into kindPerWorkout(kindId,workoutId) values(4,2);
insert into kindPerWorkout(kindId,workoutId) values(1,3);
insert into kindPerWorkout(kindId,workoutId) values(2,4);
insert into kindPerWorkout(kindId,workoutId) values(3,4);
insert into kindPerWorkout(kindId,workoutId) values(1,5);
insert into kindPerWorkout(kindId,workoutId) values(1,6);
insert into kindPerWorkout(kindId,workoutId) values(5,7);
insert into kindPerWorkout(kindId,workoutId) values(4,7);
insert into kindPerWorkout(kindId,workoutId) values(2,8);
insert into kindPerWorkout(kindId,workoutId) values(3,9);
insert into kindPerWorkout(kindId,workoutId) values(4,9);
insert into kindPerWorkout(kindId,workoutId) values(5,9);

insert into scheduales(workoutId,roomId,_dateTime,endDateTime) values(1,'11b','2022-01-28 10:00','2022-01-28 12:00');
insert into scheduales(workoutId,roomId,_dateTime,endDateTime) values(1,'1','2022-01-28 13:00','2022-01-28 13:45');
insert into scheduales(workoutId,roomId,_dateTime,endDateTime) values(9,'3c','2022-01-28 10:00','2022-01-28 11:00');
insert into scheduales(workoutId,roomId,_dateTime,endDateTime) values(7,'11b','2022-01-28 18:00','2022-01-28 19:30');
insert into scheduales(workoutId,roomId,_dateTime,endDateTime) values(3,'2','2022-01-29 11:00','2022-01-29 11:40');
insert into scheduales(workoutId,roomId,_dateTime,endDateTime) values(3,'2','2022-01-29 14:00','2022-01-29 14:45');

insert into reservations(userId,schedualeId,_date) values('ivan123',1,'2022-01-2 10:00');
update scheduales set isFree = false where schedualeId = 1;
insert into reservations(userId,schedualeId,_date) values('jovana123',2,'2022-01-05 21:50:03');
insert into reservations(userId,schedualeId,_date) values('jovana123',3,now());
insert into reservations(userId,schedualeId,_date) values('ivan123',3,now());
insert into reservations(userId,schedualeId,_date) values('jovana123',5,now());
insert into reservations(userId,schedualeId,_date) values('ivan123',5,now());
insert into reservations(userId,schedualeId,_date) values('ivan123',6,now());
update scheduales set isFree = false where schedualeId = 6;

insert into comments(workoutId,content,rating,_date,userId) values (1,'test neki tekst',4,now(),'ivan123');
insert into comments(workoutId,content,rating,_date,userId) values (1,'test neki tekst',5,now(),'jovana123');
insert into comments(workoutId,content,rating,_date,userId) values (9,'test neki tekst',2,now(),'jovana123');
insert into comments(workoutId,content,rating,_date,userId) values (2,'test neki tekst',5,now(),'jovana123');
insert into comments(workoutId,content,rating,_date) values (9,'test neki tekst',5,now());
insert into comments(workoutId,content,rating,_date) values (4,'test neki tekst',1,now());

insert into specialDates(startDate,endDate,workoutId,discount) values (now(),'2022-02-02',9,5);
insert into specialDates(startDate,endDate,workoutId,discount) values (now(),'2022-02-02',1,15);
insert into specialDates(startDate,endDate,workoutId,discount) values (now(),'2022-02-02',3,30);

insert into shoppingCart(userId ,schedualeId) values ('jovana123',3);
insert into shoppingCart(userId ,schedualeId) values ('ivan123',3);
insert into shoppingCart(userId ,schedualeId) values ('jovana123',4);
