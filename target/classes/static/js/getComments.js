$(document).ready(function (){
    var form = $("form[name = commentForm]")
    var addForm = $(".addToCart");
    fillInComments()


   addForm.submit(function (e) {
       addToCart( $(this).find("input[name='scheduleId']").val());
       e.preventDefault();
   })

    form.submit(function (e){
        fillInComments()
        e.preventDefault();
    })
})

function fillInComments(){
    var id = { id: $("form[name = commentForm] input[type = hidden]").val()};
    $.get("Comments/GetComments",id,function (comments){
        var table = $("#commentTable")
        $("#commentTable tbody").remove();
        for (var i = 0; i < comments.length; i++){
            var tbody = document.createElement("tbody")
            var tr = document.createElement("tr")
            table.append(tbody);
            tbody.appendChild(tr)
            var tdAuthor = document.createElement("td");
            var tdContent = document.createElement("td");
            var tdDate = document.createElement("td");
            var tdRating = document.createElement("td");
            tdAuthor.innerText = comments[i]["author"];
            tdContent.innerText = comments[i]["content"];
            tdDate.innerText = new Date((comments[i]["date"][0]),(comments[i]["date"][1]),(comments[i]["date"][2]),(comments[i]["date"][3]),(comments[i]["date"][4],0)).toISOString().slice(0,16).replace("T"," ");
            tdRating.innerText = comments[i]["rating"];
            tr.appendChild(tdAuthor)
            tr.appendChild(tdContent)
            tr.appendChild(tdDate)
            tr.appendChild(tdRating)
        }

    })
}

function addToCart(scheduleId){
    var params = {
        scheduleId : scheduleId
    }
    $.post("ShoppingCart/AddToCart",params,function (response){
        if (response["status"] == "ok"){
            window.location.reload()
        }else{
            alert("You allready added  schedule in that time range!")
        }
    })
}