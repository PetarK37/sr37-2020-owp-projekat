$(document).ready(function(){
    var form = $("form")
    var error = $("#errorDiv")

    form.submit(function (e){
        var password = form.find("input[name = password]").val()
        var passwordCheck = form.find("input[name = passwordCheck]").val()

        if(password != passwordCheck){
            error.find('h2:first').remove();
            var errorMsg = document.createElement("h2")
            errorMsg.style.color = "red";
            errorMsg.innerText = "passwords do not match!"
            error.append(errorMsg)
            e.preventDefault()
        }
    })
})