$(document).ready(function (){
    $.get("Comments/GetApplications",function (comments){
        var table = $("#commentTable")
        $("#commentTable tbody").remove();
        for (var i = 0; i < comments.length; i++){

            var tbody = document.createElement("tbody")
            var tr = document.createElement("tr")

            table.append(tbody);

            tbody.appendChild(tr)
            var tdAuthor = document.createElement("td");
            var tdContent = document.createElement("td");
            var tdDate = document.createElement("td");
            var tdRating = document.createElement("td");
            var tdWorkout = document.createElement("td")
            var tdAccept = document.createElement("td")
            var tdDecline = document.createElement("td")

            var accept = document.createElement("form")
            accept.setAttribute("method","post")
            accept.setAttribute("action","Comments/Accept")
            accept.setAttribute("name","acceptForm")


            var acceptBtn = document.createElement("input")
            acceptBtn.setAttribute("type","submit")
            acceptBtn.setAttribute("value","accept")

            var hidden2 = document.createElement("input")
            hidden2.setAttribute("type","hidden")
            hidden2.setAttribute("value",comments[i]["id"])
            hidden2.setAttribute("name","id")

            var decline = document.createElement("form")
            decline.setAttribute("method","post")
            decline.setAttribute("action","Comments/Decline")
            decline.setAttribute("name","declineForm")

            var declineBtn = document.createElement("input")
            declineBtn.setAttribute("type","submit")
            declineBtn.setAttribute("value","decline")

            var hidden = document.createElement("input")
            hidden.setAttribute("type","hidden")
            hidden.setAttribute("value",comments[i]["id"])
            hidden.setAttribute("name","id")

            accept.appendChild(hidden2)
            accept.appendChild(acceptBtn)
            decline.appendChild(hidden)
            decline.appendChild(declineBtn)

            tdWorkout.innerText = comments[i]["workout"]["name"];
            tdAuthor.innerText = comments[i]["author"];
            tdContent.innerText = comments[i]["content"];
            tdDate.innerText = new Date((comments[i]["date"][0]),(comments[i]["date"][1]),(comments[i]["date"][2]),(comments[i]["date"][3]),(comments[i]["date"][4],0)).toISOString().slice(0,16).replace("T"," ");
            tdRating.innerText = comments[i]["rating"];

            tdAccept.appendChild(accept)
            tdDecline.appendChild(decline)

            tr.appendChild(tdAuthor)
            tr.appendChild(tdWorkout)
            tr.appendChild(tdContent)
            tr.appendChild(tdDate)
            tr.appendChild(tdRating)
            tr.appendChild(tdAccept)
            tr.appendChild(tdDecline)
        }
    })
})